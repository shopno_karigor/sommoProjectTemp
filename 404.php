<?php
$page_slug="404";
include "views/layout/header.php";
?>
<body>
<?php include "views/layout/nav.php"; ?>

<div class="clear"> </div>
<div class="wrap">
<div class="content">
<div class="content-grids-error">
	<div class="error-page">
		<h3>404</h3>
		<h5>A Page Not Found 404 error occurred</h5>
	</div>
	<a href="#"><h4>Back to Home</h4></a>
	</div>
</div>
<div class="clear"> </div>
</div>
<?php include 'views/layout/footer.php'; ?>
