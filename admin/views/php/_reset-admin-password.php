<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Admin\Admin;
$b = new Admin();
if (isset($_POST)) {
  if (empty($_POST['resetPassword'])) {
    header("location:../../logout.php");
  }
  else {
    $b->set_data_reset_password($_POST);
    $data = $b->find_update_admin($_SESSION["userId"]);
    $passForCheck = $b->generate_admin_check_pass($data[0]['user_hash']);
    if ( ($data[0]['user_name'] == $_SESSION["username"]) && ($data[0]['user_id'] == $_SESSION['userId']) ) {
      if ($data[0]['user_pass'] == $passForCheck ) {
        $hash = $b->create_hash();
        $newUpdatepassword = $b->generate_reset_pass($hash);
        $insert = $b->reset_password($newUpdatepassword,$hash);
        if (empty($insert)) {
          $_SESSION['adminPasswordReset']="Password Reset Successfull";
          header("location:".$baseUrl."admin/views/layout/update-admin-super.php?id=".$_POST['userId']);
        }
      }else {
        $_SESSION['errorResetPassword']="Admin Password Wrong";
        header("location:".$baseUrl."admin/views/layout/update-admin-super.php?id=".$_POST['userId']);
      }
    }else {
      header("location:../../logout.php");
    }
  }
}
 ?>
