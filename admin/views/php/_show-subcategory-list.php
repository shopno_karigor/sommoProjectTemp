<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Category\Category;
$b = new Category();
$row = $b->show_subcategory_list();
foreach ($row as $value) {
?>
<tr>
  <td><?php echo $value['subcategory_id'];?></td>
  <?php
    if ($value['categorie_id'] == 'c1') {
      $category ="Men's Collection";
    }elseif ($value['categorie_id'] == 'c2') {
      $category ="Women's Collection";
    }else{
      $category ="Kid's Collection";
    }
  ?>
  <td><?php echo $category;?></td>
  <td><?php echo $value['subcategory_name'];?></td>
  <td><a href="update-subcategory.php?subcategory=<?php echo $value['subcategory_id'] ?>" class="btn btn-success">Update</a></td>
  <td><a class="btn btn-danger delete-subcategory" data-row="<?php echo $value['subcategory_id'] ?>">Delete</a></td>
</tr>
<?php
}

?>
