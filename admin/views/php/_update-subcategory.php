<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Category\Category;
$b = new Category();
if (isset($_POST)) {
  if (empty($_POST["subcategory"])) {
    $_SESSION['errorMessage']="Field Empty";
    header("location:".$baseUrl."admin/views/layout/update-subcategory.php?subcategory=".$_POST['subcategoryId']);
  }else {
    $b->setData($_POST);
    $result = $b->update();
    if (empty($result)) {
      $_SESSION['subcategorySuccess']='Subcategory Update Successful';
      header("location:".$baseUrl."admin/views/layout/update-subcategory.php?subcategory=".$_POST['subcategoryId']);
    }
  }
}

 ?>
