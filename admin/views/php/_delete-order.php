<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Order\Order;
$b = new Order();
if (isset($_POST['delete'])) {
  $id = $_POST['delete'];
  $yearMonthDay = date("Y-m-d");
  $hourSecond = date("h:i:s");
  $dateAndTime = $yearMonthDay." ".$hourSecond;
  $b->delete_from_order($id,$dateAndTime);
  $b->delete_from_invoice($id,$dateAndTime);
}
if (isset($_POST['delivery'])) {
  $id = $_POST['delivery'];
  $yearMonthDay = date("Y-m-d");
  $hourSecond = date("h:i:s");
  $dateAndTime = $yearMonthDay." ".$hourSecond;
  $b->delivery_order($id,$dateAndTime);
}
?>
