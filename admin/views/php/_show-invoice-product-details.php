<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Order\Order;
$b = new Order();
$row = $b->find_invoice_service_details($_GET['id']);
$serviceList = $row['product_item'];
$serviceArray = explode(",",$serviceList);
$serviceArray = array_count_values($serviceArray);

$j = sizeof($serviceArray);
$totalPrice = 0;
for ($i=0; $i <$j ; $i++) {
 $index = key($serviceArray);
 $row = $b->find_service_details($index);
 $quantity = $serviceArray[$index];
?>
<tr>
  <td><?php echo $i+1; ?></td>
  <td class="serviceName"><?php echo $row['products_title']; ?></td>
  <td class="serviceName"><?php echo $row['price'].' Taka'; ?></td>
  <td class="serviceDes"><?php echo $quantity; ?></td>
  <td class="servicePrice"><?php echo ($row['price']*$quantity)." Taka"; ?></td>
</tr>
<?php
$totalPrice = $totalPrice+($row['price']*$quantity);
 unset($serviceArray[$index]);
  }
 ?>
