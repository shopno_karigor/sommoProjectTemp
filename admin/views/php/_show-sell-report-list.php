<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Sell\Sell;
$b = new Sell();
$row = $b->show_all_paid_orders();
foreach ($row as  $value) {
  $product = $b->find_order_products($value['order_id']);
  $productList = $product[0]['product_item'];
  $productArray = explode(",",$productList);
  $productArray = array_count_values($productArray);
  $j = sizeof($productArray);
  for ($i=0; $i <$j ; $i++) {
    $index = key($productArray);
    $row = $b->find_products_details($index);
    if (!empty($row)) {
      $quantity = $productArray[$index];
      echo '<tr>';
        echo '<td>'.$row[0]['product_id'].'</td>';
        echo '<td>'.$row[0]['products_title'].'</td>';
        echo '<td>'.$quantity.'</td>';
        echo '<td>'.substr($value['created_at'],0,10).'</td>';
      echo '</tr>';
    }
    unset($productArray[$index]);
 }
}
?>
