<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Sell\Sell;
$b = new Sell();
$row = $b->find_sold_products();
$generalProduct = $b->find_all_products();

$allProductList = '';
$generalProductList = '';
// parent foreach
foreach ($row as  $value) {
  $allProductList = $allProductList.",".$value['product_item'];
}
$allProductArray = explode(",",$allProductList);
foreach ($generalProduct as  $value) {
  $generalProductList = $generalProductList.",".$value['product_id'];
}
$generalProductArray = explode(",",$generalProductList);

$differentItems=array_diff($generalProductArray,$allProductArray);
// echo "<pre>";
// print_r($differentItems);
// die();

// for sold product
$allProductArray = array_count_values($allProductArray);
arsort($allProductArray);
$arrlength=sizeof($allProductArray);
for($i=1;$i<$arrlength;$i++)
  {
    $index = key($allProductArray);
    $sellQuantity = $allProductArray[$index];
    $productsDetails = $b->find_products_details($index);
    if (!empty($productsDetails)) {
      $productsCategory = $b->find_products_category($productsDetails[0]['categorie']);
      $remainQuantity = $productsDetails[0]['quantity']-$sellQuantity;
    ?>
    <tr>
      <td><?php echo $productsDetails[0]['product_id'];?></td>
      <td><?php echo $productsDetails[0]['products_title'];?></td>
      <td><?php echo $productsCategory;?></td>
      <td><?php echo $productsDetails[0]['price'];?></td>
      <td><?php echo $productsDetails[0]['quantity'];?></td>
      <td><?php echo $sellQuantity;?></td>
      <td><?php echo $remainQuantity;?></td>
      <?php
      if ($remainQuantity <= 0) {
        echo '<td><a class="btn btn-danger">Sold out</a></td>';
      }else {
        echo '<td><a class="btn btn-primary">Instock</a></td>';
      }
      ?>
    </tr>
    <?php
    }
    unset($allProductArray[$index]);
  }
// for unsold product
$differentItems = array_count_values($differentItems);
arsort($differentItems);
$arrlength=sizeof($differentItems);
for($i=0;$i<$arrlength;$i++)
  {
    $index = key($differentItems);
    $sellQuantity = 0;
    $productsDetails = $b->find_products_details($index);
    if (!empty($productsDetails)) {
    $productsCategory = $b->find_products_category($productsDetails[0]['categorie']);
    $remainQuantity = $productsDetails[0]['quantity']-$sellQuantity;
    ?>
    <tr>
      <td><?php echo $productsDetails[0]['product_id'];?></td>
      <td><?php echo $productsDetails[0]['products_title'];?></td>
      <td><?php echo $productsCategory;?></td>
      <td><?php echo $productsDetails[0]['price'];?></td>
      <td><?php echo $productsDetails[0]['quantity'];?></td>
      <td><?php echo $sellQuantity;?></td>
      <td><?php echo $remainQuantity;?></td>
      <?php
      if ($remainQuantity <= 0) {
        echo '<td><a class="btn btn-danger">Sold out</a></td>';
      }else {
        echo '<td><a class="btn btn-primary">Instock</a></td>';
      }
      ?>
    </tr>
    <?php
    }
    unset($differentItems[$index]);
  }
?>
