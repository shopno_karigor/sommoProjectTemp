<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Order\Order;
$b = new Order();
$row = $b->show_all_orders();
foreach ($row as $value) {
  echo '<tr>';
    echo '<td>'.$value['order_id'].'</td>';
    echo '<td>'.$value['invoice_id'].'</td>';
    echo '<td>'.substr($value['created_at'],0,10).'</td>';
    echo '<td><a class="btn btn-primary" target="_blank" href="invoice.php?id='.$value['invoice_id'].'">Invoice</a></td>';
    if ($value['status']!="0"){
      echo '<td><a class="text-success">Delivered</a></td>';
      echo '<td><a class="">--</a></td>';
    }else {
      echo '<td><a class="btn btn-success deliver-order" data-row="'.$value['order_id'].'">Pending</a></td>';
      echo '<td><a class="btn btn-danger delete-order" data-row="'.$value['order_id'].'">Delete</a></td>';
    }
  echo '</tr>';
}
?>
