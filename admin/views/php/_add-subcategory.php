<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Category\Category;
$b = new Category();
if (isset($_POST)) {
  if ($_POST["categoryId"]=='select') {
    $_SESSION['errorMessage']="Select Category";
    header("location:".$baseUrl."admin/views/layout/add-subcategory.php");
  }else {
    $b->setData($_POST);
    $last= $b->find_last_subcategory();
    $last = $last+1;
    $sucategory_id = $_POST['categoryId']."s".$last;
    $result = $b->insert($sucategory_id);
    if (empty($result)) {
      $_SESSION['subcategorySuccess']='Subcategory Insert Successful';
      header("location:".$baseUrl."admin/views/layout/add-subcategory.php");
    }
  }
}

 ?>
