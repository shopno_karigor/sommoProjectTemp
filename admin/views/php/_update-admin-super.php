<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Admin\Admin;
$b = new Admin();
$b->find_update_admin();
if (isset($_POST)) {
  if ($_POST['adminStatus'] == $_POST['currentStatus'] ) {
    $_SESSION['adminSuccess']="Status remain same";
    header("location:".$baseUrl."admin/views/layout/update-admin-super.php?id=".$_POST['userId']);
  }elseif ($_POST['adminStatus'] == "select") {
    $_SESSION['errorMessage']="Select admin area";
    header("location:".$baseUrl."admin/views/layout/update-admin-super.php?id=".$_POST['userId']);
  }else {
    $b->setData($_POST);
    $insert =$b->index();
    if (empty($insert)) {
      $_SESSION['adminSuccess']="Admin Status Change To ".$_POST['adminStatus'];
      header("location:".$baseUrl."admin/views/layout/update-admin-super.php?id=".$_POST['userId']);
    }
  }
}
$userName = $row[0]['user_name'];
$email = $row[0]['email'];
$status = $row[0]['status'];
$token = $row[0]['token'];

?>
