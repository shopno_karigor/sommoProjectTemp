<?php
include_once ("../../vendor/autoload.php");
use App\TopData\TopData;
$b = new TopData();

$orderDetails = $b->find_all_order();
$paid =0;
$pending =0;
foreach ($orderDetails as $value) {
  if ($value['status']=="1") {
    $paid++;
  }else {
    $pending++;
  }
}
$deliveredQuantiry = $paid;
$pendingQuantiry = $pending;

$todaySell = 0;
$todayInvoice = $b->today_invoice(date("Y-m-d"));
foreach ($todayInvoice as $value) {
  $productList = $value['product_item'];
  $productArray = explode(",",$productList);
  $productArray = array_count_values($productArray);
  $discount = 0;
  $total = 0;
  $j = sizeof($productArray);
  for ($i=0; $i <$j ; $i++) {
   $index = key($productArray);
   $row = $b->find_product_details($index);
   $quantity = $productArray[$index];
   $total = $total+($row['price']*$quantity);
   unset($productArray[$index]);
 }
  $discountPrice = $total*($discount/100);
  $totalAmount = $total-$discountPrice;
  $todaySell = $todaySell+floor($totalAmount);
  $totalAmount =0;
}
 ?>
