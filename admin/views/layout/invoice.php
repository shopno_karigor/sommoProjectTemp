<?php
$page_slug="invoice";
include '../php/_header.php';
include 'header.php';
include '../php/_show-order-details.php';
?>
</head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="<?php echo $baseUrl.'views/layout/home.php' ?>" class="site_title"><i class="fa fa-tachometer"></i> <span><?php echo $websiteName ?></span></a>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="../../assets/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $_SESSION["username"];?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- Sidebar menue -->
            <?php include 'sidebar.php'; ?>
            <!-- Sidebar menue -->
          </div>
        </div>
        <!-- top navigation -->
        <?php include 'top-navigation.php'; ?>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Invoice</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Invoice<small><?php echo $invoiceId;?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" id="invoiceBody">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                              <img src="../../assets/images/logo1.png" alt="">
                              <small class="pull-right" id="generationDate" >Date: <?php echo date("d")."/".date("m")."/20".date('y'); ?></small>
                          </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          From
                          <address>
                              <strong>Online Sell Platform</strong>
                              <br>Rupnogor R/A, Mirpur-2, Dhaka
                              <br>Phone: 01988888888
                              <br>Email: info@onlinesell.com
                              <br>Rupnogor R/A, Mirpur-2, Dhaka
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                            <strong id="clientName"><?php echo $name;?></strong>
                            <br> <span id="companyPhone">Phone: <?php echo $phone;?></span>
                            <br><span id="companyEmail">Email: <?php echo $email;?></span>
                            <br><span id="companyAddress">Address: <?php echo $address;?></span>
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Invoice: <?php echo $invoiceId;?></b>
                          <br>
                          <br>
                          <b>Order ID: </b><span id="orderId"><?php echo $orderId;?></span>
                          <br>
                          <b>Order Date: </b><span id="orderDate"><?php echo $orderDate;?></span>
                          <br>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped" id="productListTable">
                            <thead>
                              <tr>
                                <th>Serial</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php include '../php/_show-invoice-product-details.php'; ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Payment Details:</p>
                          <p><strong>Payment Method: </strong><span id="paymentType">Cash on delivery</span> </p>
                         <br>
                         <p><strong>Admin Signature and Date: </strong><span>_______________________</span> </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Amount Total</p>
                          <div class="table-responsive">
                            <table class="table" >
                              <tbody>
                                <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td id="subtotal"><?php echo $totalPrice." Taka"; ?></td>
                                </tr>
                                <tr>
                                  <th><?php echo $reduceType; ?> ( <span id="discount"><?php echo $reduceAmount."%"; ?></span> )</th>
                                  <td id="discountAmount"><?php echo $totalPrice*($reduceAmount/100)." Taka"; ?></td>
                                </tr>
                                <tr>
                                  <th>Total:</th>
                                  <td id="total"><?php echo floor($totalPrice-($totalPrice*($reduceAmount/100))).".00 Taka"; ?></td>
                                </tr>
                              </tbody>
                            </table>
                            <br>
                            <br>
                            <p><strong>Customer Signature and Date: </strong><span>_______________________</span> </p>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->


                    </section>
                  </div>
                </div>
              </div>
            </div>
            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                <button class="btn btn-default" onclick="printDiv('invoiceBody');"><i class="fa fa-print"></i> Print</button>
                <button id="generatePdf" data-invoice="<?php echo $invoiceId ?>" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

      </div>
    </div>


    <!-- Footer -->
    <?php include 'footer.php'; ?>
    <!-- Footer -->
