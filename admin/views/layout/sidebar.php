            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="home.php">Dashboard</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cart-arrow-down"></i>All Orders<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="order-list.php">All Order List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-file-pdf-o"></i>Sells Manager<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="sell-report.php">Sells Report</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-database"></i>Product Inventory<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="inventory-list.php">Inventory List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> Admin Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php
                      if ($_SESSION['adminStatus'] == "Super" || $_SESSION['adminStatus'] == "Prime"){
                        echo '<li><a href="create-admin.php">Create Admin</a></li>';
                        echo '<li><a href="manage-admin-super.php">Manage Admin</a></li>';
                      }
                      if ($_SESSION['adminStatus'] == "Moderator"){
                        echo '<li><a href="manage-admin-moderator.php">Manage Admin</a></li>';
                      }
                      ?>
                      <li><a href="update-admin.php">Update Admin</a></li>
                    </ul>
                  </li>
                  <?php if (($_SESSION['adminStatus'] == "Super") || ($_SESSION['adminStatus'] == "Prime")){ ?>
                    <li><a><i class="fa fa-sitemap"></i>Category Manager <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="add-subcategory.php">Add Subcategory</a></li>
                        <li><a href="manage-subcategory.php">Manage Subcategory</a></li>
                      </ul>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu
