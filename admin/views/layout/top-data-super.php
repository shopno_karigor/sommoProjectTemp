<?php include '../php/_top-data-super.php'; ?>
 <!-- top tiles -->
 <div class="row tile_count">
   <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top text-primary"><i class="fa fa-cart-arrow-down"></i> Order Delivered</span>
     <div class="count text-primary"><?php echo $deliveredQuantiry; ?></div>
   </div>
   <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top red"><i class="fa fa-hourglass-half"></i> Order Pending</span>
     <div class="count red"><?php echo $pendingQuantiry; ?></div>
   </div>
   <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top light-blue"><i class="fa fa-money"></i> Today Sell (QAR)</span>
     <div class="count light-blue"><?php echo number_format(floor($todaySell),2); ?></div>
   </div>
 </div>
 <!-- /top tiles -->
