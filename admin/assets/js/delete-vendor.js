$(document).ready(function () {
  $('.delete-vendor').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Customer !"))
    {
      var dataToSend = { contentId: rowValue };
      $.post("../php/_delete-vendor.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });
});
