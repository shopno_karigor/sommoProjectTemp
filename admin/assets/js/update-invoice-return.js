$(document).ready(function () {
  $('.remove-product').click(function (event) {
    if (window.confirm("Remove total quantity"))
    {
      var _this = $(this);
      _this.parent().parent().remove();
    }
  });

  $('.update-invoice').click(function (event) {
    var invoiceId = $(this).attr("data-invoice");
      prevTotal = $("#total").attr("data-total");
      tableLength = $("#productListTable tbody tr").length;
    for (var i = 1; i <= tableLength; i++) {
      $("#productListTable tbody tr:nth-child("+i+") td:last").attr("id","row"+i);
    }
    var productString='';
        newPrice='';
    for (var i = 1; i <= tableLength; i++) {
      var productId = $("#row"+i).attr("data-product");
          quantity = $("#row"+i).prev().children("input").val();
          quantityNumber = Number(quantity);
          price = $("#row"+i).prev().prev().attr("data-price");
          priceNumber = Number(price);
          rowPrice = priceNumber*quantityNumber;
          productSubStr='';
      for (var j = 0; j < quantity; j++) {
        productSubStr=productSubStr+","+productId;
      }
      productString=productString+productSubStr;
      newPrice=Number(newPrice)+Number(rowPrice);
    }
    if (Number(prevTotal)>=Number(newPrice)) {
      var priceDif = Number(prevTotal)-Number(newPrice);
      productString = productString.substr(1);
      if (productString!='') {
        var dataToSend = { invoice: invoiceId,lessAmount: priceDif,products: productString };
        $.post("../php/_update-invoice.php", dataToSend, function(data, status){
            if(status === 'success')
            {
               location.reload();
            }
          });
      }else {
        alert("Information Invalid");
      }
    }else {
      alert("Information Invalid");
    }
  });
});
