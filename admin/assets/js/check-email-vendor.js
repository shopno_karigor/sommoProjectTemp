$(document).ready(function() {
  $("#emailCheckVendor").keyup(function() {
    var userName = $("#emailCheckVendor").val();
    if (userName) {
      var dataToSend = { content: userName };
      $.post("../php/_check-email-vendor.php", dataToSend, function(data, status){

          if(status === 'success')
          {
            if (data) {
              $("#errorEmailVendor").css("display","block")
               $("#btnCreateVendor").css("display","none")
            }
            else {
              $("#errorEmailVendor").css("display","none")
              $("#btnCreateVendor").css("display","block")
            }
          }
        });
    }
  });

});
