$(document).on("click", '.delete-cost', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Delete This Company Cost !"))
  {
    var dataToSend = { contentId: rowValue };
    $.post("../php/_delete-cost.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           _this.parent().parent().remove();
        }
      });
  }
});
