
$(document).ready(function () {
  $('#vendorSearch').bind('input', function() {
    var _this = $(this);
    var categoryId = $('#vendorSearch').val();
    var dataToSend = { searchKey: categoryId };
    $.post('../php/_find-create-order-vendor.php', dataToSend, function (data,status) {
      if (status === "success") {
        $('#vendorName').html(data);
        $('#vendorEmail').val(' ');
        $('#vendorPhone').val(' ');
        $('#vendorAddress').val(' ');
        $('#vendorCompany').val(' ');
      }
    });
  });

  $('#vendorName').change(function() {
    var id = $('#vendorName').find(":selected").attr("value");
    var dataToSend = { vendorId: id };
    if (id == 'select') {
      $('#vendorEmail').val(' ');
      $('#vendorPhone').val(' ');
      $('#vendorAddress').val(' ');
      $('#vendorCompany').val(' ');
    }else {
      $.post("../php/_find-vendor-details-order.php", dataToSend, function(data, status){
          if(status === 'success')
          {
            var newData = data.split('>');
            $('#vendorEmail').val(newData[0]);
            $('#vendorPhone').val(newData[1]);
            $('#vendorAddress').val(newData[2]);
            $('#vendorCompany').val(newData[3]);
          }
        });
    }
  });

  $('#orderCategory').change(function() {
    var id = $('#orderCategory').find(":selected").attr("value");
    var dataToSend = { categoryId: id };
    if (id == 'select') {
      alert("Order category error");
      $('#searchProductId').val('');
      $('#serviceId').val('');
      $('#price').val('');
      $('#quantity').val('');
    }else {
      $.post("../php/_show-product-by-category.php", dataToSend, function(data, status){
          if(status === 'success')
          {
            $('#service').html(data);
            $('#searchProductId').val('');
            $('#serviceId').val('');
            $('#price').val('');
            $('#quantity').val('');
          }
        });
    }
  });

  $('#searchProductId').bind('input', function() {
    var _this = $(this);
    var productId = $('#searchProductId').val();
    var categoryId = $('#orderCategory').find(":selected").val();
    var dataToSend = { searchKey: productId, category: categoryId };
    if (categoryId != "select") {
      $.post('../php/_show-product-list-order.php', dataToSend, function (data,status) {
        if (status === "success") {
          $('#service').html(data);
          $('#serviceId').val('');
          $('#price').val('');
          $('#quantity').val('');
        }
      });
    }else {
      alert("Select Category First");
    }
  });

  $('#service').change(function (event) {
    var _this = $(this);
    var price = $('#service option:selected').attr('data-price');
    var serviceId = $('#service option:selected').val();
    $('#serviceId').val(serviceId);
    $('#price').val(price);
    $('#quantity').val('');
  });

  $('.add-item').click(function (event) {
    var s = $('#service').val();
    var price = $('#price').val();
    var quantity = $('#quantity').val();
    if (s=='select' || price==='' || quantity==='') {
      alert("Invalide Input");
    }else {
      var service = $('#service option:selected').text();
      var serviceId = $('#serviceId').val();

      $("#productCart tr:last").after('<tr><td class="col-md-2 col-sm-2 col-xs-12">'+serviceId+'</td><td class="col-md-5 col-sm-5 col-xs-12">'+service+'</td><td class="col-md-2 col-sm-2 col-xs-12">'+price+'</td><td class="col-md-2 col-sm-2 col-xs-12">'+quantity+'</td><td><a data-item="'+serviceId+'" class="btn btn-danger remove-cart" >Remove</a></td></tr>')
      var dataToSend = { itemId: serviceId, qnt: quantity };
      $.post('../php/_add-to-cart.php', dataToSend, function (data,status) {
        if (status === "success") {
        }
      });

    }
  });

  $(document).on("click",".remove-cart",function (event) {
    var _this = $(this);
    var item = $(this).attr("data-item");
    var dataToSend = { itemId: item };
    $.post('../php/_delete-from-cart.php', dataToSend, function (data,status) {
      if (status === "success") {
        _this.parent().parent().remove();
      }
    });
  });

  $('#create-order').click(function (event) {
    var a = $('#vendorName').val();
    var b = $('#orderCategory').val();
    var c = $('#createdBy').val();
    var d = $('#phone').val();
    var e = $('#address').val();
    if (d=='') {
      $('#phone').css("border","1px solid red");
    }else {
      $('#phone').css("border","1px solid #333");
    }
    if (e=='') {
      $('#address').css("border","1px solid red");
    }else {
      $('#address').css("border","1px solid #333");
    }

    if (a!='' && d!='' && e!='') {
      if (window.confirm("Confirm Order !"))
      {
        var dataToSend = { vendor: a, category: b, createdBy: c, phone: d, address: e };
        $.post('../php/_create-order.php', dataToSend, function (data,status) {
          if (status === "success") {
            alert("Order successfull");
          }
        });
      }
    }else {
      alert("Information missing");
    }
  });

});
