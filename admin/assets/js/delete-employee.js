$(document).ready(function () {
  $('.delete-employee').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Employee !"))
    {
      var dataToSend = { contentId: rowValue };
      $.post("../php/_delete-employee.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });
});
