$(document).ready(function () {
  $('.delete-subcategory').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Subcategory !"))
    {
      var dataToSend = { contentId: rowValue };
      $.post("../php/_delete-subcategory.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });
});
