
$(document).ready(function () {
  $('.create-account').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Create account !"))
    {
      var dataToSend = { createAccount: rowValue };
      $.post("../php/_manage-accounts.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             location.reload();
          }
        });
    }
  });

  $('.deactive-account').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Deactive this account !"))
    {
      var dataToSend = { deactiveAccount: rowValue };
      $.post("../php/_manage-accounts.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             location.reload();
          }
        });
    }
  });

  $('.active-account').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Active this account !"))
    {
      var dataToSend = { activeAccount: rowValue };
      $.post("../php/_manage-accounts.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             location.reload();
          }
        });
    }
  });

});
