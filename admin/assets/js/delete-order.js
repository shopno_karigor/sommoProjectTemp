$(document).on("click", '.deliver-order', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Confirm Delivery"))
  {
    var dataToSend = { delivery: rowValue };
    $.post("../php/_delete-order.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           location.reload();
        }
      });
  }
});
$(document).on("click", '.delete-order', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Confirm Delete"))
  {
    var dataToSend = { delete: rowValue };
    $.post("../php/_delete-order.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           _this.parent().parent().remove();
        }
      });
  }
});
