$(document).ready(function () {
  $('.receive-balance').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Receive transfer"))
    {
      var dataToSend = { receive: rowValue };
      $.post("../php/_balance-receive-reject.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });

  $('.reject-balance').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Reject transfer"))
    {
      var dataToSend = { reject: rowValue };
      $.post("../php/_balance-receive-reject.php", dataToSend, function(data, status){
          if(status === 'success')
          {
              _this.parent().parent().remove();
          }
        });
    }
  });

});
