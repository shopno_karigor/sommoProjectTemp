$(document).ready(function() {
  $('#single_cal1').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_1"
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
  $('#single_cal4').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_4"
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });

  $('#searchByDate').click(function (event) {
    var fullDate = new Date();
    var date = fullDate.getDate();
    var month  = fullDate.getMonth();
    month = month +1;
    var year = fullDate.getYear();
        year = year.toString();
    var year = year.slice(1);
        year = Number(year);

    var date1 = $("#single_cal1").val();
    var date2 = $("#single_cal4").val();
    var searchKey = $("#searchKey").val();
    var page = $("#pageMarker").val();
    var yearSlice = date2.slice(8, 10);
        yearSlice = Number(yearSlice);
    var monthSlice = date2.slice(0,2);
        monthSlice = Number(monthSlice);
    var dateSlice = date2.slice(3, 5);
        dateSlice = Number(dateSlice);

    var year1Slice = date1.slice(8, 10);
        year1Slice = Number(year1Slice);
    var month1Slice = date1.slice(0,2);
        month1Slice = Number(month1Slice);
    var date1Slice = date1.slice(3, 5);
        date1Slice = Number(date1Slice);

    var flag;
    if (date1 == "" || date2 == "") {
      alert("Invalid Input");
      flag = 1;
    }else {
      if ( year >= yearSlice && year >= year1Slice) {
        if (month > monthSlice && month > month1Slice) {
          flag = 0;
        }else{
          if (monthSlice > month1Slice && date >= dateSlice) {
            flag = 0;
          }else {
            if (date >= dateSlice && date >= date1Slice) {
              flag = 0;
            }else {
              alert("Invalid Input");
              flag = 1;
            }
          }
        }
      }else {
        alert("Invalid Input");
        flag = 1;
      }
    }

    yearSlice = "20"+yearSlice.toString();
    if (monthSlice<10) {
      monthSlice = monthSlice.toString();
      monthSlice = "0"+monthSlice;
    }else {
      monthSlice = monthSlice.toString();
    }
    if (dateSlice<10) {
      dateSlice = dateSlice.toString();
      dateSlice = "0"+dateSlice;
    }else {
      dateSlice = dateSlice.toString();
    }

    year1Slice = "20"+year1Slice.toString();
    if (month1Slice<10) {
      month1Slice = month1Slice.toString();
      month1Slice = "0"+month1Slice;
    }else {
      month1Slice = month1Slice.toString();
    }
    if (date1Slice<10) {
      date1Slice = date1Slice.toString();
      date1Slice = "0"+date1Slice;
    }else {
      date1Slice = date1Slice.toString();
    }

    var dateFormate1 =year1Slice+"-"+month1Slice+"-"+date1Slice+" "+"00:00:01";
    var dateFormate2 =yearSlice+"-"+monthSlice+"-"+dateSlice+" "+"23:59:59";
    // alert(page);
    if (flag == 0) {
      var dataToSend = { searchDate1: dateFormate1, searchDate2: dateFormate2, key: searchKey, pageMarker: page };
      $.post("../php/_show-custom-search-table.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             $("#custom-search-table tbody").html(data);
             var totatRow = $("#custom-search-table tbody tr").length;
             $("#table-row-info").text("Showing "+totatRow+" entries");
          }
        });
    }else {
      $("#custom-search-table tbody").html('');
    }

  });
});
