$(document).ready(function() {
  $("#emailCheck").keyup(function() {
    var userName = $("#emailCheck").val();
    if (userName) {
      var dataToSend = { content: userName };
      $.post("../php/_check-email.php", dataToSend, function(data, status){

          if(status === 'success')
          {
            if (data) {
              $("#errorEmail").css("display","block")
               $("#btnCreateAdmin").css("display","none")
            }
            else {
              $("#errorEmail").css("display","none")
              $("#btnCreateAdmin").css("display","block")
            }
          }
        });
    }
  });

});
