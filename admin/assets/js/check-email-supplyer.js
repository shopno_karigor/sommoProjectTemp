$(document).ready(function() {
  $("#emailCheckSupplyer").keyup(function() {
    var email = $("#emailCheckSupplyer").val();
    if (email) {
      var dataToSend = { content: email };
      $.post("../php/_check-email-supplyer.php", dataToSend, function(data, status){

          if(status === 'success')
          {
            if (data) {
              $("#errorEmailSupplyer").css("display","block")
               $("#btnCreateSupplyer").css("display","none")
            }
            else {
              $("#errorEmailSupplyer").css("display","none")
              $("#btnCreateSupplyer").css("display","block")
            }
          }
        });
    }
  });

});
