$(document).on("click", '.delete-product', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Delete This Product !"))
  {
    var dataToSend = { contentId: rowValue };
    $.post("../php/_delete-product.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           _this.parent().parent().remove();
        }
      });
  }
});

$(document).on("click", '.hide-product', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Hide from stock !"))
  {
    var dataToSend = { contentIdHide: rowValue };
    $.post("../php/_maintain-stock.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           location.reload();
        }
      });
  }
});

$(document).on("click", '.show-product', function(event) {
  var _this = $(this);
  var rowValue = $(this).attr('data-row');
  if (window.confirm("Show in stock !"))
  {
    var dataToSend = { contentIdShow: rowValue };
    $.post("../php/_maintain-stock.php", dataToSend, function(data, status){
        if(status === 'success')
        {
           location.reload();
        }
      });
  }
});
