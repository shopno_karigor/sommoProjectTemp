<?php
namespace App\Order;
use PDO;
require_once '../../db/dbconnection.php';

class Order
{
  public function show_all_orders($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from primary_order WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function delivery_order($value='',$dateAndTime)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  primary_order SET status = :s, updated_at = :ua WHERE order_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          's' => 1,
          'ua' => $dateAndTime
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_invoice_details($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from invoice WHERE invoice_id='$value' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_invoice_service_details($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from primary_order WHERE invoice_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_service_details($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from products_web WHERE product_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function delete_from_order($value='',$dateAndTime)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  primary_order SET deleted_at = :da WHERE order_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $dateAndTime
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function delete_from_invoice($value='',$dateAndTime)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  invoice SET deleted_at = :da WHERE order_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $dateAndTime
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
