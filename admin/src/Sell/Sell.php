<?php
namespace App\Sell;
use PDO;
require_once '../../db/dbconnection.php';
class Sell
{

  // Inventory
  public function find_sold_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from primary_order WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_all_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from products_web WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_products_details($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from products_web WHERE product_id='$value' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_products_category($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from category WHERE categorie_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0]['categorie_name'];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

  // Sell Report
  public function show_all_paid_orders($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from primary_order WHERE deleted_at='0000-00-00 00:00:00' AND status='1'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_order_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from primary_order WHERE deleted_at='0000-00-00 00:00:00' AND order_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
