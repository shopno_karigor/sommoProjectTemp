<?php
namespace App\Category;
use PDO;
require_once '../../db/dbconnection.php';

class Category
{
  private $categoryId ='';
  private $subcategoryId ='';
  private $subcategory ='';
  private $createdAt='';
  private $updatedAt='';

  public function setData($value='')
  {
    if (array_key_exists("categoryId",$value)) {
      $this->categoryId=$value['categoryId'];
    }
    if (array_key_exists("subcategoryId",$value)) {
      $this->subcategoryId =$value['subcategoryId'];
    }
    if (array_key_exists("subcategory",$value)) {
      $this->subcategory=$value['subcategory'];
    }
    if (array_key_exists("createdAt",$value)) {
      $this->createdAt=$value['createdAt'];
    }
    if (array_key_exists("updatedAt",$value)) {
      $this->updatedAt=$value['updatedAt'];
    }
    return $this;
  }
  public function find_last_subcategory($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from subcategory";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert($sucategory_id)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO subcategory (subcategory_id,categorie_id,subcategory_name,created_at) Values(:si,:ci,:sn,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'si' => $sucategory_id,
          'ci' => $this->categoryId,
          'sn' => $this->subcategory,
          'ca' => $this->createdAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_subcategory_list($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from subcategory WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_single_subcategory($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from subcategory WHERE deleted_at='0000-00-00 00:00:00' AND subcategory_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function update($value='')
  {
    try{
      $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
      $query = "UPDATE  subcategory SET subcategory_name = :n,updated_at = :ua WHERE subcategory_id='$this->subcategoryId'";
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(
        'n' => $this->subcategory,
        'ua' => $this->updateAt
      ));
      $data = $stmt->fetchAll();
      return $data;
    }catch (PDOException $e){
      echo 'Error: '. $e->getMessage();
    }
  }
  public function delete($value='',$deletedAt)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  subcategory SET deleted_at = :da WHERE subcategory_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $deletedAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}

 ?>
