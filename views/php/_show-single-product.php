<?php
include_once ("_header.php");
include "../../vendor/autoload.php";
use App\Products\Products;
$b = new Products();
$product_id = $_GET['product'];
$row = $b->show_single_product($product_id);

if (!empty($row)) {
  $productId =$row[0]['product_id'];
  $title =$row[0]['products_title'];
  $image =$row[0]['product_image'];
  $image2 =$row[0]['product_image2'];
  if ($row[0]['show_price'] == '0') {
    $price =$row[0]['price'];
    $oldPrice =$row[0]['previous_price'];
  }else {
    $price ="N/A";
    $oldPrice ="N/A";
  }

  $description =$row[0]['product_des'];
  $stock =$row[0]['instock'];
  $categoryId =$row[0]['categorie'];
  $quantity = $row[0]['quantity'];
  $soldProducts = $b->find_sold_products();
  $allProductList = '';
  foreach ($soldProducts as  $value) {
    $allProductList = $allProductList.",".$value['product_item'];
  }
  $allProductArray = explode(",",$allProductList);
  $allProductArray = array_count_values($allProductArray);

  if (array_key_exists($product_id,$allProductArray)) {
    $soldQuantity=$allProductArray[$product_id];
  }else {
    $soldQuantity=0;
  }

  if ($soldQuantity >= $quantity) {
    $inStock = "Sold Out";
  }else {
    $inStock = "Instock";
  }

  if ($categoryId=='c1') {
    $categoryName = "Men's Collection";
  }elseif ($categoryId=='c2') {
    $categoryName = "Women's Collection";
  }else {
    $categoryName = "Kid's Collection";
  }

}

?>
