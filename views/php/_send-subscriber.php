<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Mail\Mail;
$b = new Mail();
if (isset($_POST)) {
  if (empty($_POST['subscriber'])) {
    $_SESSION['errorMessage']="Field empty";
    header("location:".$baseUrl."views/layout/contact.php");
  }
  else {
    $b->setData($_POST);
    $yearMonthDay = date("Y-m-d");
    $hourSecond = date("h:i:s");
    $dateAndTime = $yearMonthDay." ".$hourSecond;
    $result = $b->insert_subscriber($dateAndTime);
    if (empty($result)) {
      $_SESSION['mailSuccess']='Email added for News-Latter ';
      header("location:".$baseUrl."views/layout/contact.php");
    }
  }
}
 ?>
