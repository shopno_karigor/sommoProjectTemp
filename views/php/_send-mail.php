<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Mail\Mail;
$b = new Mail();

if (isset($_POST)) {
  if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['message']) ) {
    $_SESSION['errorMessage']="Field empty";
    header("location:".$baseUrl."views/layout/contact.php");
  }
  else {
    $b->setData($_POST);
    $last= $b->find_last_mail();
    $last = $last+1;
    $mail_id = "mail".$last;
    $yearMonthDay = date("Y-m-d");
    $hourSecond = date("h:i:s");
    $dateAndTime = $yearMonthDay." ".$hourSecond;
    $result = $b->insert($mail_id,$dateAndTime);
    if (empty($result)) {
      $_SESSION['mailSuccess']='Thanks for contact us';
      header("location:".$baseUrl."views/layout/contact.php");
    }
  }
}
 ?>
