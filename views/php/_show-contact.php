<?php
include_once ("_header.php");
if (file_exists("vendor/autoload.php")) {
  include "vendor/autoload.php";
}else {
  include "../../vendor/autoload.php";
}
use App\Contact\Contact;
$b = new Contact();
$row = $b->show_contact();
if (!empty($row)) {
  $address =$row[0]['address'];
  $email =$row[0]['email'];
  $phone =$row[0]['phone'];
  $phone2 =$row[0]['hotline'];
  $fax =$row[0]['fax'];
  $fbLink =$row[0]['facebook'];
  $twitterLink =$row[0]['twitter'];
}
?>
