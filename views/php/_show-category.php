<?php
include_once ("_header.php");
if (file_exists("vendor/autoload.php")) {
  include "vendor/autoload.php";
}else {
  include "../../vendor/autoload.php";
}
use App\Category\Category;
$b = new Category();
$row = $b->show_category();
foreach ($row as $value) {
  $subcategory = $b->show_subcategory($value['categorie_id']);
  if (!empty($subcategory)) {?>
    <li class="has-sub" data-stat="close">
      <a href="<?php echo $baseUrl.'views/layout/single-store.php?category='.$value['categorie_id'];?>"><?php echo $value['categorie_name']; ?> &nbsp;<span class="fa fa-chevron-down"></span></a>
      <ul class="has-close">
        <?php foreach ($subcategory as  $subcategoryValue){ ?>
          <li><a href="<?php echo $baseUrl.'views/layout/single-store.php?subcategory='.$subcategoryValue['subcategory_id'];?>"><?php echo $subcategoryValue['subcategory_name']; ?></a></li>
        <?php } ?>
      </ul>
    </li>
<?php
  }else {
?>
  <li><a href="<?php echo $baseUrl.'views/layout/single-store.php?category='.$value['categorie_id'];?>"><?php echo $value['categorie_name']; ?></a></li>
<?php
  }
}
?>
