<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Products\Products;
$b = new Products();
if(!empty($_GET['page'])){
    $currentPage= $_GET['page']-1;
}
else{
    $currentPage= 0;
}
if (isset($category)) {
  $noOfItem = $b->find_single_store_product_number($category);
  $limit= 12;
  $noOfPage = ceil($noOfItem / $limit );
  $offset = $limit * $currentPage;
  $row = $b->show_single_store_product($category,$limit,$offset);
}else {
  $noOfItem = $b->find_single_store_product_number_sub($subcategory);
  $limit= 12;
  $noOfPage = ceil($noOfItem / $limit );
  $offset = $limit * $currentPage;
  $row = $b->show_single_store_product_sub($subcategory,$limit,$offset);
}


$i=1;
foreach ($row as $value) {
?>
<li id="<?php echo $i; ?>">
  <img src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$value['product_image']?>" class="items" alt="" />
  <br clear="all" />
  <div><?php echo $value['products_title']?></div>
</li>
<?php
$i++;
}
// Detail Boxes for above four li
$i=1;
foreach ($row as $value) {
?>
<div class="detail-view" id="<?php echo 'detail-'.$i; ?>">
  <div class="close" align="right">
    <a href="javascript:void(0)">x</a>
  </div>
  <img src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$value['product_image2']?>" class="detail_images" alt="" />
  <div class="detail_info">
    <label class='item_name'><?php echo $value['products_title']?></label>
    <br clear="all" />
    <p>
      <?php echo $value['product_des']?>
      <br clear="all" /><br clear="all" />
      <?php if ($value['show_price']=='0') {
        echo '<span class="price">'.$value['price'].' Taka</span>';
      }else {
        echo '<span class="price">N/A</span>';
      } ?>
    </p>
    <br clear="all" />
    <a href="<?php echo $baseUrl.'views/layout/single.php?product='.$value['product_id'];?>"><button  class="add-to-cart-button-store">Show Details</button></a>
  </div>
</div>
<?php
$i++;
}
?>
