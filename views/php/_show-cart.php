<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Products\Products;
$b = new Products();
if (isset($_SESSION['cart'])) {
  $cart_data = array_count_values($_SESSION['cart']);
  $arraySize = sizeof($cart_data);
  for ($i=0; $i < $arraySize; $i++) {
    $index = key($cart_data);
    $quantity = $cart_data[$index];
    unset($cart_data[$index]);
    $product= $b->find_checkout_product($index);
    echo '<div class="cart-box col-lg-12 col-md-12">';
      echo '<table>';
        echo '<tbody>';
    foreach ($product as $value) {
    ?>
    <tr>
      <td class="col-lg-1"><img class="img-responsive" src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$value['product_image']; ?>" alt=""></td>
      <td class="col-lg-3"><p><?php echo $value['products_title']; ?></p></td>
      <td class="col-lg-2"><p><?php echo $value['price'].' Taka'; ?></p></td>
      <td class="col-lg-2"><?php echo $quantity.' Pis'; ?></td>
      <td class="col-lg-1"><a class="btn btn-danger remove-cart" data-item="<?php echo $value['product_id']; ?>">Remove</a></td>
    </tr>
    <?php
    }
        echo '</tbody>';
      echo '</table>';
    echo '</div>';
  }
}else {
  echo '<h2 class="cart-empty text-danger">Cart Empty</h2>';
}
?>
