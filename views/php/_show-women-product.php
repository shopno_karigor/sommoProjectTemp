<?php
include_once ("_header.php");
include_once ("vendor/autoload.php");
use App\Products\Products;
$b = new Products();
$row = $b->show_women_product();
foreach ($row as $value) {
?>
<div class="grid_1_of_4 images_1_of_4 products-info">
   <a href="<?php echo $baseUrl.'views/layout/single.php?product='.$value['product_id'];?>"><img src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$value['product_image']?>"></a>
   <a href="<?php echo $baseUrl.'views/layout/single.php?product='.$value['product_id'];?>"><?php echo substr($value['product_des'],0,60)?></a>
   <?php
    if ($value['show_price']=='0') {
      echo '<h3>'.$value['price'].' Taka</h3>';
    }else {
      echo '<h3>N/A</h3>';
    }
    ?>
   <ul>
    <li><a  class="cart add-item" data-item="<?php echo $value['product_id']; ?>"> </a></li>
    <li><a  class="i" href="<?php echo $baseUrl.'views/layout/single.php?product='.$value['product_id'];?>"> </a></li>
   </ul>
</div>
<?php
}
?>
