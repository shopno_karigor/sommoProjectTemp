<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Order\Order;
$b = new Order();
if (isset($_POST)) {
  if (empty($_SESSION['cart'])) {
    $_SESSION['errorMessage']="Information missing";
    header("location:".$baseUrl."views/layout/order.php");
  }
  else {
    $b->setData($_POST);
    $orderId = $b->create_orderId();
    $invoiceId = $b->create_invoiceId();
    $iteam = $b->arrayToString($_SESSION['cart']);
    $insert = $b->insert_order($orderId,$invoiceId,$iteam);
    $insert = $b->insert_invoice($orderId,$invoiceId);
    unset($_SESSION['cart']);
    $_SESSION['orderSuccess']="Order Success";
    header("location:".$baseUrl."views/layout/order.php");
  }
}
 ?>
