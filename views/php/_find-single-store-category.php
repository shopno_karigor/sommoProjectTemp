<?php
include_once ("_header.php");
if (file_exists("vendor/autoload.php")) {
  include "vendor/autoload.php";
}else {
  include "../../vendor/autoload.php";
}
use App\Category\Category;
$b = new Category();
if (isset($_GET['category'])) {
  $category = $_GET['category'];
  if ($category=='c1') {
    $categoryName = "Men's Collection";
  }elseif ($category=='c2') {
    $categoryName = "Women's Collection";
  }else {
    $categoryName = "Kid's Collection";
  }
}else {
  $subcategory = $_GET['subcategory'];
  $subcategoryName = $b->find_subcategory_name($subcategory);
  if (!empty($subcategoryName)) {
    $categoryName = $subcategoryName[0]['subcategory_name'];
  }
}

 ?>
