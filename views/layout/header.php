<?php
session_start();
if (file_exists("../php/_header.php")) {
  include "../php/_header.php";
}else {
  include "views/php/_header.php";
}
 ?>
 <!DOCTYPE HTML>
 <html>
 	<head>
 		<title>Online Sell Platform</title>
    <link href="<?php echo $baseUrl.'assets/images/icon1.png'; ?>" rel="icon" type="image/x-icon" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="<?php echo $baseUrl.'assets/css/font-awesome.css'; ?>" type="text/css"  media="all" />
    <link href="<?php echo $baseUrl.'assets/css/bootstrap.css'; ?>" rel="stylesheet" type="text/css"  media="all" />
 		<link href="<?php echo $baseUrl.'assets/css/style.css'; ?>" rel="stylesheet" type="text/css"  media="all" />
 		<link href='//fonts.googleapis.com/css?family=Londrina+Solid|Coda+Caption:800|Open+Sans' rel='stylesheet' type='text/css'>
 		<link rel="stylesheet" href="<?php echo $baseUrl.'assets/css/responsiveslides.css'; ?>">
    <link href="<?php echo $baseUrl.'assets/css/style1.css'; ?>" rel="stylesheet" />
    <?php if ($page_slug=="single"){ ?>
    <link rel="stylesheet" href="<?php echo $baseUrl.'assets/css/flexslider.css'; ?>" type="text/css" media="screen" />
    <?php } ?>

 		<script src="<?php echo $baseUrl.'assets/js/jquery.min.js'; ?>"></script>
    <script src="<?php echo $baseUrl.'assets/js/responsiveslides.min.js'; ?>"></script>
    <script src="<?php echo $baseUrl.'assets/js/order-processing.js'; ?>"></script>
 		<script src="<?php echo $baseUrl.'assets/js/custom.js'; ?>"></script>
    <?php if ($page_slug=="home"){ ?>
      <script>
        // You can also use "$(window).load(function() {"
          $(function () {

            // Slideshow 1
            $("#slider1").responsiveSlides({
              maxwidth: 1600,
              speed: 600
            });
      });
     </script>
    <?php } ?>
    <?php if ($page_slug=="store" || $page_slug=="fetured" || $page_slug=="single-store" || $page_slug=="search"){ ?>
      <script type="text/javascript" src="<?php echo $baseUrl.'assets/js/jquery.livequery.js'; ?>"></script>
      <script type="text/javascript">
  			$(document).ready(function() {

  				var Arrays=new Array();
  				// this is for 2nd row's li offset from top. It means how much offset you want to give them with animation
  				var single_li_offset 	= 200;
  				var current_opened_box  = -1;
  				$('#wrap li').click(function() {
  					var thisID = $(this).attr('id');
  					var $this  = $(this);
  					var id = $('#wrap li').index($this);

  					if(current_opened_box == id) // if user click a opened box li again you close the box and return back
  					{
  						$('#wrap .detail-view').slideUp('slow');
  						return false;
  					}
  					$('#cart_wrapper').slideUp('slow');
  					$('#wrap .detail-view').slideUp('slow');

  					// save this id. so if user click a opened box li again you close the box.
  					current_opened_box = id;

  					var targetOffset = 0;

  					// below conditions assumes that there are four li in one row and total rows are 4. How ever if you want to increase the rows you have to increase else-if conditions and if you want to increase li in one row, then you have to increment all value below. (if(id<=3)), if(id<=7) etc

  					if(id<=3)
  						targetOffset = 0;
  					else if(id<=7)
  						targetOffset = single_li_offset;
  					else if(id<=11)
  						targetOffset = single_li_offset*2;
  					else if(id<=15)
  						targetOffset = single_li_offset*3;

  					$("html:not(:animated),body:not(:animated)").animate({scrollTop: targetOffset}, 800,function(){
  						$('#wrap #detail-'+thisID).slideDown(500);
  						return false;
  					});

            $('.close a').click(function() {
          		$('#wrap .detail-view').slideUp('slow');
          	});

  				});

  			});
  			function include(arr, obj) {
  			  for(var i=0; i<arr.length; i++) {
  			    if (arr[i] == obj) return true;
  			  }
  			}
  			function getpos(arr, obj) {
  			  for(var i=0; i<arr.length; i++) {
  			    if (arr[i] == obj) return i;
  			  }
  			}
  		</script>
    <?php } ?>
    <?php if ($page_slug=="single"){ ?>
      <script src="<?php echo $baseUrl.'assets/js/jqzoom.pack.1.0.1.js'; ?>" type="text/javascript"></script>
      <script src="<?php echo $baseUrl.'assets/js/imagezoom.js'; ?>"></script>
      <script defer src="<?php echo $baseUrl.'assets/js/jquery.flexslider.js'; ?>"></script>
      <script>
        // Can also be used with $(document).ready()
        $(window).load(function() {
          $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
          });
        });
      </script>
      <!----->
      <script>
        $(document).ready(function(){
          $(".menu_body").hide();
          //toggle the componenet with class menu_body
          $(".menu_head").click(function(){
            $(this).next(".menu_body").slideToggle(600);
            var plusmin;
            plusmin = $(this).children(".plusminus").text();

            if( plusmin == '+')
            $(this).children(".plusminus").text('-');
            else
            $(this).children(".plusminus").text('+');
          });
        });
      </script>
    <?php } ?>
 	</head>
  <?php
  if (file_exists("views/php/_show-contact.php")) {
    include "views/php/_show-contact.php";
  }else {
    include "../php/_show-contact.php";
  }
  ?>
