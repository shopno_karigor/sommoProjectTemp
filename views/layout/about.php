<?php
$page_slug="about";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
<div class="wrap">
	<div class="content">
  	<div class="about">
  		<h4>About Us</h4>
  		<div class="section group">
				<div class="col_1_of_3 span_1_of_3 about-frist">
					<h3>Online Sell Platform</h3>
					<p>Online Sell Platform is considered to be one of the leading companies in Bangladesh in the field of trading with Men, Women and Kids dresses.</p>
				</div>
				<div class="col_1_of_3 span_1_of_3 about-centre">
					<h3>Our Mission</h3>
					<a><img src="<?php echo $baseUrl.'assets/images/logo.png'; ?>"></a>
					<p>We hope to increase our relationships with the best dealers and buyers of worldwide.</p>
				</div>
				<div class="col_1_of_3 span_1_of_3 quites">
					<h3>Testimonials</h3>
					<?php include '../php/_show-testimonial.php'; ?>
				</div>
				<div class="clear"> </div>
				<div class="section group">
  	</div>
			</div>
 		</div>
	</div>
</div>
<div class="clear"> </div>
<?php include 'footer.php'; ?>
