<?php
$page_slug="search";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
<div class="wrap">
	<div class="content">
		<div class="content-grids">
			<div align="left" style="min-height:800px;">
				<div id="wrap" align="center">
					<a class="show_cart">Search Result</a>
					<ul>
						<?php include '../php/_show-search-product.php'; ?>
					</ul>
					<br clear="all" />
				</div>
			</div>
			<?php include '../php/_show-search-pagination.php'; ?>
		</div>
	</div>
	<?php include 'sidebar.php'; ?>
</div>
<div class="clear"> </div>
</div>
<?php include 'footer.php'; ?>
