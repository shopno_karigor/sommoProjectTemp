<div class="wrap">
  <div class="header">
  <div class="search-bar">
    <form method="get" action="<?php echo $baseUrl.'views/layout/search.php'; ?>" autocomplete="off">
      <input type="text" name="searchValue"><input type="submit" value="Search" />
    </form>
  </div>
  <div class="clear"> </div>
  <div class="logo">
    <a href="<?php echo $baseUrl;?>"><img src="<?php echo $baseUrl.'assets/images/logo1.png'; ?>" title="logo" /></a>
  </div>
  <div class="header-top-nav">
    <ul>
      <li><a href="#"><span>Email&nbsp;&nbsp;: </span></a><label> &nbsp;<?php echo $email; ?></label></li>
      <li><a href="#"><span>Hot Line&nbsp;&nbsp;: </span></a><label> &nbsp;<?php echo $phone2; ?></label></li>
    </ul>
  </div>
  <div class="clear"> </div>
</div>
</div>
<div class="clear"> </div>
<div class="top-header">
  <div class="wrap">
    <div class="top-nav">
      <ul>
        <li><a href="<?php echo $baseUrl ?>">Home</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/about.php';?>">About Us</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c1';?>">Men's Collection</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c2';?>">Women's Collection</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c3';?>">Kid's Collection</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/contact.php';?>">Contact Us</a></li>
        <li><a href="<?php echo $baseUrl.'views/layout/cart.php';?>">Cart</a></li>
      </ul>
    </div>
    <div class="clear"> </div>
</div>
</div>
