<div class="footer">
  <div class="wrap">
  <div class="section group">
    <div class="col_1_of_4 span_1_of_4">
      <h3><a href="<?php echo $baseUrl.'views/layout/about.php';?>" style="color:#ffffff">Our Info</a></h3>
      <p style="color:#ffffff">Online Sell Platform is considered to be one of the leading companies in Bangladesh in the field of trading with Men, Women and Kids dresses.</p>
    </div>
    <div class="col_1_of_4 span_1_of_4">
      <h3><a href="<?php echo $baseUrl.'views/layout/about.php';?>" style="color:#ffffff">Our Mission</a></h3>
      <p style="color:#ffffff">We hope to increase our relationships with the best dealers and buyers of worldwide.</p>
      <p style="color:#ffffff">With our experience and your collaboration, we hope to get further with new ideas, and new deals, to join the links of business partnership between Asia, USA, the Middle East, Africa and Europe!</p>
    </div>
    <div class="col_1_of_4 span_1_of_4">
      <h3><a href="<?php echo $baseUrl.'views/layout/contact.php';?>" style="color:#ffffff">Store Location</a></h3>
      <p style="color:#ffffff"><?php echo $address; ?></p>
      <h3><a href="<?php echo $baseUrl.'views/layout/contact.php';?>" style="color:#ffffff">Order-online</a></h3>
      <p style="color:#ffffff"><?php echo $phone; ?></p>
      <p style="color:#ffffff"><?php echo $phone2; ?></p>
    </div>
    <div class="col_1_of_4 span_1_of_4 footer-lastgrid">
      <h3 style="color:#ffffff">News-Letter</h3>
      <form method="post" action="../php/_send-subscriber.php">
        <input type="email" name="subscriber" required><input type="submit" value="go" />
      </form>
      <h3 style="color:#ffffff">Follow Us:</h3>
       <ul>
        <li><a href="<?php echo $fbLink; ?>"><img src="<?php echo $baseUrl.'assets/images/twitter.png'; ?>" title="twitter" />Twitter</a></li>
        <li><a href="<?php echo $twitterLink; ?>"><img src="<?php echo $baseUrl.'assets/images/facebook.png'; ?>" title="Facebook" />Facebook</a></li>
       </ul>
    </div>
  </div>
</div>
<div class="clear"> </div>
<div class="wrap">
<div class="copy-right">
  <p>&copy; <?php echo "20".date("y");?> Online Sell Platform All Rights Reserved | Design by  <a href="">Online Sell Platform</a></p>
</div>
</div>
</div>
</body>
</html>
