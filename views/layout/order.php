<?php
$page_slug="register";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
  <div class="wrap">
    <div class="content">
    	<div class="section group">
			<div class="col span_2_of_3">
			  <div class="contact-form">
			  	<h2>Order Information</h2>
          <h3 style="text-align:center; color:red" class="text-danger">
              <?php
              if (isset($_SESSION['errorMessage'])) {
                  echo $_SESSION['errorMessage'];
                  unset($_SESSION['errorMessage']);
              }
              ?>
          </h3>
          <h3 style="text-align:center;" class="text-success">
              <?php
              if (isset($_SESSION['orderSuccess'])) {
                  echo $_SESSION['orderSuccess'];
                  unset($_SESSION['orderSuccess']);
              }
              ?>
          </h3>
				    <form method="post" action="../php/_create-order.php">
				    	<div>
					    	<span><label>Name</label></span>
					    	<span><input type="text" name="name" placeholder="Name"></span>
					    </div>
              <div>
					     	<span><label>Phone</label></span>
					    	<span><input type="text" name="phone" required placeholder="Order Phone"></span>
					    </div>
					    <div>
					    	<span><label>Address</label></span>
					    	<span><input type="text" name="address" required placeholder="Address"></span>
					    </div>
					    <div>
					     	<span><label>Email</label></span>
					    	<span><input type="text" name="email" required placeholder="Email"></span>
					    </div>
              <div>
					    	<span><input type="hidden" name="createAt" required value="<?php echo date("Y-m-d")." ".date("h:i:s");?>"></span>
					    </div>
					   <div>
               <span><button type="submit" class="btn btn-success">Submit</button></span>
					  </div>
				    </form>
			    </div>
				</div>
		  </div>
		  <div class="clear"> </div>
		</div>
		<div class="clear"> </div>
  </div>
</div>
<?php include 'footer.php'; ?>
