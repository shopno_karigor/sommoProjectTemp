<?php
$page_slug="single-store";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
<div class="wrap">
	<div class="content">
		<div class="content-grids">
			<div align="left" style="min-height:800px;">
				<div id="wrap" align="center">
					<?php include '../php/_find-single-store-category.php'; ?>
					<a class="show_cart"><?php echo $categoryName; ?></a>
					<ul>
						<?php include '../php/_show-single-store-product.php'; ?>
					</ul>
					<br clear="all" />
				</div>
			</div>
			<?php include '../php/_show-pagination.php'; ?>
		</div>

	</div>
	<?php include 'sidebar.php'; ?>
</div>
<div class="clear"> </div>
</div>
<?php include 'footer.php'; ?>
