<?php
$page_slug="cart";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
<div class="wrap">
	<div class="content">
		<div class="content-grids">
			<div align="left">
				<?php
        if(!isset($_SESSION['cart'])) {
          echo '<h2 class="cart-empty text-danger">Cart Empty</h2>';
        }else {
          include '../php/_show-cart.php';
        }
         ?>
			</div>
			<div class="order-box col-lg-12" align="left">
				<a href="order.php" class="btn btn-primary">Proceed To Order</a>
			</div>
		</div>
	</div>
	<?php include 'sidebar.php'; ?>
</div>
<div class="clear"> </div>
</div>
<?php include 'footer.php'; ?>
