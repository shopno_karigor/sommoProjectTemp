<?php
$page_slug="home";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<?php include "banner.php"; ?>
<div class="clear"> </div>
<div class="wrap">
  <div class="content">
    <div class="top-3-grids">
      <div class="section group">
        <?php include 'views/php/_show-top-product.php'; ?>
        <div class="grid_1_of_3 images_1_of_3">
          <a href=""><img src="<?php echo $baseUrl.'admin-web/assets/images/top-product/'.$topProduct1;?>"></a>
          <h3>Latest Design </h3>
        </div>
        <div class="grid_1_of_3 images_1_of_3 second">
          <a href=""><img src="<?php echo $baseUrl.'admin-web/assets/images/top-product/'.$topProduct2;?>"></a>
          <h3>New Arrival </h3>
        </div>
        <div class="grid_1_of_3 images_1_of_3 theree">
          <a href=""><img src="<?php echo $baseUrl.'admin-web/assets/images/top-product/'.$topProduct3;?>"></a>
          <h3>Signature Quality </h3>
        </div>
      </div>
    </div>
    <div class="content-grids">
      <h4>Men's Collection</h4>
      <div class="section group">
        <?php include 'views/php/_show-men-product.php'; ?>
        <div class="clear"> </div>
        <a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c1';?>"><button  class="add-to-cart-button">Show More</button></a>
      </div>
      <h4>Womens's Collection</h4>
      <div class="section group">
        <?php include 'views/php/_show-women-product.php'; ?>
        <div class="clear"> </div>
        <a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c2';?>"><button  class="add-to-cart-button">Show More</button></a>
      </div>
      <h4>Kids's Collection</h4>
      <div class="section group">
        <?php include 'views/php/_show-kid-product.php'; ?>
        <div class="clear"> </div>
        <a href="<?php echo $baseUrl.'views/layout/single-store.php?category=c3';?>"><button  class="add-to-cart-button">Show More</button></a>
      </div>
    </div>
    <?php include 'sidebar.php'; ?>
  </div>
  <div class="clear"> </div>
</div>
<?php include 'footer.php'; ?>
