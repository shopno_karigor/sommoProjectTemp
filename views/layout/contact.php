<?php
$page_slug="contact";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
  <div class="wrap">
    <div class="content">
    	<div class="section group">
			<div class="col span_1_of_3">
				<div class="contact_info">
		    	 	<h2>Find Us Here</h2>
		    	 		<div class="map">
                <iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.1924182609805!2d90.35480391498251!3d23.811755784559193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c12015382851%3A0x3ceca92fcf1a72d2!2sBangladesh+University+of+Business+%26+Technology!5e0!3m2!1sen!2sbd!4v1531288071793" ></iframe>
                <br>
                <small><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.1924182609805!2d90.35480391498251!3d23.811755784559193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c12015382851%3A0x3ceca92fcf1a72d2!2sBangladesh+University+of+Business+%26+Technology!5e0!3m2!1sen!2sbd!4v1531288071793" style="color:#666;text-align:left;font-size:12px">View Larger Map</a></small>
				   		</div>
    				</div>
    			<div class="company_address">
			     	<h2>Company Information :</h2>
					  <p><?php echo $address; ?></p>
					  <p>Bangladesh</p>
			   		<p>Phone: <?php echo "&nbsp;".$phone; ?></p>
			   		<p>Fax: <?php echo "&nbsp;".$fax; ?></p>
			 	 	  <p>Email: <span><a><?php echo $email; ?></a></span></p>
			   		<p>Follow on: <span><a href="<?php echo $fbLink ?>">Facebook</a></span>, <span><a href="<?php echo $twitterLink ?>">Twitter</a></span></p>
			   </div>
			</div>
			<div class="col span_2_of_3">
			  <div class="contact-form">
			  	<h2>Contact Us</h2>
            <h3 style="text-align:center; color:red" class="text-danger">
                <?php
                if (isset($_SESSION['errorMessage'])) {
                    echo $_SESSION['errorMessage'];
                    unset($_SESSION['errorMessage']);
                }
                ?>
            </h3>
            <h3 style="text-align:center;" class="text-success">
                <?php
                if (isset($_SESSION['mailSuccess'])) {
                    echo $_SESSION['mailSuccess'];
                    unset($_SESSION['mailSuccess']);
                }
                ?>
            </h3>
				    <form method="post" action="../php/_send-mail.php">
				    	<div>
					    	<span><label>NAME</label></span>
					    	<span><input type="text" value="" name="name"></span>
					    </div>
					    <div>
					    	<span><label>E-MAIL</label></span>
					    	<span><input type="text" value="" name="email"></span>
					    </div>
					    <div>
					     	<span><label>MOBILE.NO</label></span>
					    	<span><input type="text" value="" name="phone"></span>
					    </div>
					    <div>
					    	<span><label>MESSAGE</label></span>
					    	<span><textarea name="message"> </textarea></span>
					    </div>
					   <div>
					   		<span><input type="submit" value="Submit"></span>
					  </div>
				    </form>
			    </div>
				</div>
		  </div>
		  <div class="clear"> </div>
		</div>
		<div class="clear"> </div>
  </div>
</div>
<?php include 'footer.php'; ?>
