<?php
$page_slug="single";
include "header.php";
?>
<body>
<?php include "nav.php"; ?>
<div class="clear"> </div>
<div class="wrap">
	<div class="content">
		<div class="content-grids">
			<div class="details-page">
				<div class="back-links">
					<ul>
						<li><a href="<?php echo $baseUrl; ?>">Home</a><img src="<?php echo $baseUrl.'assets/images/arrow.png'; ?>" alt=""></li>
						<li><a>Product-Details</a><img src="<?php echo $baseUrl.'assets/images/arrow.png'; ?>" alt=""></li>
					</ul>
				</div>
			</div>
			<?php include_once '../php/_show-single-product.php'; ?>
			<div class="detalis-image">
				<div class="flexslider">
					<ul class="slides">
						<li data-thumb="<?php echo $baseUrl.'admin-web/assets/images/products/'.$image; ?>">
							<div class="thumb-image"> <img src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$image; ?>" data-imagezoom="true" class="img-responsive" alt="" /> </div>
						</li>
						<li data-thumb="<?php echo $baseUrl.'admin-web/assets/images/products/'.$image2; ?>">
							<div class="thumb-image"> <img src="<?php echo $baseUrl.'admin-web/assets/images/products/'.$image2; ?>" data-imagezoom="true" class="img-responsive" alt="" /> </div>
						</li>
					</ul>
				</div>
			</div>
			<div class="detalis-image-details">
				<div class="details-categories">
					<ul>
						<li><h3>Category:</h3></li>
						<li class="active1"><a href="<?php echo $baseUrl.'views/layout/single-store.php?category='.$categoryId;?>"><span><?php echo $categoryName; ?></span></a></li>
					</ul>
				</div>
				<div class="brand-value">
					<h3>Product-Complete Details With Value</h3>
					<div class="left-value-details">
						<br>
						<p>Product Title : <?php echo $title ?></p>
		  			<ul>
		  				<li>Price:</li>
		  				<li><span><?php echo $oldPrice." Taka"; ?></span></li>
		  				<li><h5><?php echo $price." Taka"; ?></h5></li>
		  			</ul>
						<br>
						<a class="btn btn-success add-item" data-item="<?php echo $productId; ?>">Add to Cart</a>
					</div>
					<div class="right-value-details">
		  			<a><?php echo $inStock; ?></a>
					</div>
					<div class="clear"> </div>
				</div>
				<div class="brand-history">
					<h3>Description :</h3>
					<p><?php echo substr($description,0,200)."..."; ?></p>
				</div>
				<div class="clear"> </div>
			</div>
			<div class="clear"> </div>
			<div class="menu_container">
				<p class="menu_head">About Product<span class="plusminus">+</span></p>
					<div class="menu_body" style="display: none;">
					<p><?php echo $description; ?></p>
					</div>
			</div>
		</div>
	</div>
	<?php include 'sidebar.php'; ?>
</div>
<div class="clear"> </div>
</div>
<?php include 'footer.php'; ?>
