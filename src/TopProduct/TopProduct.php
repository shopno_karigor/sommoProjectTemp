<?php
namespace App\TopProduct;
use PDO;
require_once 'db/dbconnection.php';

class TopProduct
{
  public function show_top_product($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from top_product";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
