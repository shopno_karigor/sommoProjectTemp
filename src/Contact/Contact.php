<?php
namespace App\Contact;
use PDO;
if (file_exists("db/dbconnection.php")) {
  include_once "db/dbconnection.php";
}else {
  include_once "../../db/dbconnection.php";
}
class Contact
{
  public function show_contact($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from contact";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}
 ?>
