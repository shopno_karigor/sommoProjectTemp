<?php
namespace App\Order;
use PDO;
require_once '../../db/dbconnection.php';

class Order
{
  private $orderId ='';
  private $invoiceId='';

  private $name='';
  private $phone='';
  private $address='';
  private $email='';
  private $createAt='';

  // insert order
  public function setData($value='')
  {
    if (array_key_exists("name",$value)) {
      $this->name=$value['name'];
    }
    if (array_key_exists("phone",$value)) {
      $this->phone=$value['phone'];
    }
    if (array_key_exists("address",$value)) {
      $this->address=$value['address'];
    }
    if (array_key_exists("email",$value)) {
      $this->email=$value['email'];
    }
    if (array_key_exists("createAt",$value)) {
      $this->createAt=$value['createAt'];
    }
    return $this;
  }
  public function create_orderId($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from primary_order";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $lastOrder =$data[0][0];
        $lastOrder =$lastOrder+1;
        $yearMonthDay = date("Y-m-d");
        $hourSecond = date("h:i:s");
        $dateAndTime = $yearMonthDay." ".$hourSecond;
        $orderId = "O".$lastOrder."-".substr("$dateAndTime",2,2).substr("$dateAndTime",5,2).substr("$dateAndTime",8,2);
        return $orderId;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function create_invoiceId($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from invoice";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $lastInvoice =$data[0][0];
        $lastInvoice =$lastInvoice+1;
        $yearMonthDay = date("Y-m-d");
        $hourSecond = date("h:i:s");
        $dateAndTime = $yearMonthDay." ".$hourSecond;
        $invoiceId = substr("$dateAndTime",2,2).substr("$dateAndTime",5,2).substr("$dateAndTime",8,2).sprintf('%06d', $lastInvoice);
        return $invoiceId;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert_order($orderId,$invoiceId,$iteam)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO primary_order (order_id,invoice_id,product_item,created_at) Values(:oi,:ii,:p,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'oi' => $orderId,
          'ii' => $invoiceId,
          'p' =>  $iteam,
          'ca' => $this->createAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function arrayToString($array)
  {
    $s = sizeof($array);
    $string = $array[0].',';
    for ($i=1; $i < $s; $i++) {
      $string = $string.$array[$i].',';
    }
    return substr($string,0,-1);
  }
  // insert invoice
  public function insert_invoice($orderId,$invoiceId)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO invoice (order_id,invoice_id,name,phone,address,email,created_at) Values(:oi,:ii,:n,:p,:a,:e,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'oi' => $orderId,
          'ii' => $invoiceId,
          'n' => $this->name,
          'p' =>  $this->phone,
          'a' =>  $this->address,
          'e' =>  $this->email,
          'ca' => $this->createAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}

 ?>
