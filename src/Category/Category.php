<?php
namespace App\Category;
use PDO;
if (file_exists("db/dbconnection.php")) {
  include_once "db/dbconnection.php";
}else {
  include_once "../../db/dbconnection.php";
}
class Category
{
  public function show_category($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from category WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_subcategory($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from subcategory WHERE deleted_at='0000-00-00 00:00:00' AND categorie_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_subcategory_name($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from subcategory WHERE deleted_at='0000-00-00 00:00:00' AND subcategory_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}
 ?>
