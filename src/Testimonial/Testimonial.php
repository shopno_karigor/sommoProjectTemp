<?php
namespace App\Testimonial;
use PDO;
require_once '../../db/dbconnection.php';

class Testimonial
{
  public function show_testimonial($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from testimonial WHERE deleted_at='0000-00-00 00:00:00' AND view='1'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}
 ?>
