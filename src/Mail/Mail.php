<?php
namespace App\Mail;
use PDO;
require_once '../../db/dbconnection.php';
class Mail
{
  private $name ='';
  private $email ='';
  private $phone ='';
  private $message ='';
  private $subscriber ='';
  private $created_at='';

  public function setData($value='')
  {
    if (array_key_exists("name",$value)) {
      $this->name=$value['name'];
    }
    if (array_key_exists("email",$value)) {
      $this->email=$value['email'];
    }
    if (array_key_exists("phone",$value)) {
      $this->phone=$value['phone'];
    }
    if (array_key_exists("message",$value)) {
      $this->message=$value['message'];
    }
    if (array_key_exists("subscriber",$value)) {
      $this->subscriber=$value['subscriber'];
    }
    return $this;
  }
  public function find_last_mail($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from mail";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert($mailId,$dateAndTime)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO  mail (mail_id,name,email,phone,message,created_at) values(:mi,:n,:e,:p,:m,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'mi' => $mailId,
          'n' => $this->name,
          'e' => $this->email,
          'p' => $this->phone,
          'm' => $this->message,
          'ca' => $dateAndTime
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert_subscriber($dateAndTime)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO  subscriber (email,created_at) values(:e,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'e' => $this->subscriber,
          'ca' => $dateAndTime
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}

 ?>
