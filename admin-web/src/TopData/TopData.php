<?php
namespace App\TopData;
use PDO;
include_once '../../db/dbconnection.php';
class TopData
{
  public function total_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT count(product_id) FROM  products_web WHERE deleted_at='0000-00-00 00:00:00' AND NOT categorie='f1'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function featured_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT count(product_id) FROM  products_web WHERE deleted_at='0000-00-00 00:00:00' AND categorie='f1'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function total_subscriber($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT count(id) FROM  subscriber WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function today_mail($date='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT count(mail_id) FROM  mail WHERE created_at LIKE '$date%' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
