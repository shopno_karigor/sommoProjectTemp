<?php
namespace App\Banner;
use PDO;
require_once '../../db/dbconnection.php';

class Banner
{
  private $createAt='';
  private $updateAt='';
  private $deleteAt='';


  public function setData($value='')
  {
    if (array_key_exists("createAt",$value)) {
      $this->createAt=$value['createAt'];
    }
    if (array_key_exists("updateAt",$value)) {
      $this->updateAt=$value['updateAt'];
    }
    return $this;
  }
  public function delete($value='',$deletedAt)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  banner SET deleted_at = :da WHERE id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $deletedAt
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_last_banner($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from banner";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert($imageName='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO banner (banner_image,created_at) Values(:bi,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'bi' => $imageName,
          'ca' => $this->createAt
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_banner($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from banner WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}



 ?>
