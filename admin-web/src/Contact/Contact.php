<?php
namespace App\Contact;
use PDO;
require_once '../../db/dbconnection.php';

class Contact
{
  private $address ='';
  private $email ='';
  private $phone ='';
  private $hotline ='';
  private $fax ='';
  private $facebook ='';
  private $twitter ='';


  private $updateAt='';

  public function setData($value='')
  {
    if (array_key_exists("address",$value)) {
      $this->address=$value['address'];
    }
    if (array_key_exists("email",$value)) {
      $this->email=$value['email'];
    }
    if (array_key_exists("phone",$value)) {
      $this->phone=$value['phone'];
    }
    if (array_key_exists("hotline",$value)) {
      $this->hotline=$value['hotline'];
    }
    if (array_key_exists("fax",$value)) {
      $this->fax=$value['fax'];
    }
    if (array_key_exists("facebookLink",$value)) {
      $this->facebook=$value['facebookLink'];
    }
    if (array_key_exists("twitterLink",$value)) {
      $this->twitter=$value['twitterLink'];
    }

    if (array_key_exists("updateAt",$value)) {
      $this->updateAt=$value['updateAt'];
    }

    return $this;

  }
  public function find_contact($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from contact";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function update()
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  contact SET address = :a,email = :e,phone = :p,hotline = :h,fax = :fa,facebook = :f,twitter = :t,updated_at = :ua WHERE id='1'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'a' => $this->address,
          'e' => $this->email,
          'p' => $this->phone,
          'h' => $this->hotline,
          'fa' => $this->fax,
          'f' => $this->facebook,
          't' => $this->twitter,
          'ua' => $this->updateAt
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}



 ?>
