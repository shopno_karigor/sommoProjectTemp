<?php
namespace App\Testimonial;
use PDO;
require_once '../../db/dbconnection.php';

class Testimonial
{
  private $description='';
  private $authore='';

  private $createAt='';
  private $updateAt='';
  private $deleteAt='';

  public function setData($value='')
  {
    if (array_key_exists("descr",$value)) {
      $this->description=$value['descr'];
    }
    if (array_key_exists("authore",$value)) {
      $this->authore=$value['authore'];
    }
    if (array_key_exists("createAt",$value)) {
      $this->createAt=$value['createAt'];
    }
    if (array_key_exists("updateAt",$value)) {
      $this->updateAt=$value['updateAt'];
    }
    return $this;
  }
  public function find_last_testimonial($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT MAX(id) from testimonial";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0][0];

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert($testimonial_id)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO testimonial (testimonial_id,description,authore,created_at) Values(:ti,:de,:au,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'ti' => $testimonial_id,
          'de' => $this->description,
          'au' => $this->authore,
          'ca' => $this->createAt
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_testimonial($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from testimonial WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function active_testimonial($value='')
  {
    try{
      $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
      $query = "UPDATE  testimonial SET view = :v WHERE testimonial_id='$value'";
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(
        'v' => '1'
      ));
      $data = $stmt->fetchAll();
      return $data;

    }catch (PDOException $e){
      echo 'Error: '. $e->getMessage();
    }
  }
  public function hide_testimonial($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  testimonial SET view = :v WHERE testimonial_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'v' => '0'
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function delete($value='',$deletedAt)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  testimonial SET deleted_at = :da WHERE testimonial_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $deletedAt
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}

 ?>
