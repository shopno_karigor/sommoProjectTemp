<?php
namespace App\Subscriber;
use PDO;
require_once '../../db/dbconnection.php';
class Subscriber
{
  public function show_subscriber_list()
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * FROM  subscriber";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
}
 ?>
