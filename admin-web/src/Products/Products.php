<?php
namespace App\Products;
use PDO;
require_once '../../db/dbconnection.php';

class Products
{
  private $productId ='';
  private $title ='';
  private $price ='';
  private $previousPrice ='';
  private $quantity ='';
  private $productDes ='';
  private $category ='';
  private $subcategory ='';

  private $createdAt='';
  private $updateAt='';

  public function setData($value='')
  {
    if (array_key_exists("productId",$value)) {
      $this->productId=$value['productId'];
    }
    if (array_key_exists("title",$value)) {
      $this->title=$value['title'];
    }
    if (array_key_exists("price",$value)) {
      $this->price=$value['price'];
    }
    if (array_key_exists("previousPrice",$value)) {
      $this->previousPrice=$value['previousPrice'];
    }
    if (array_key_exists("quantity",$value)) {
      $this->quantity=$value['quantity'];
    }
    if (array_key_exists("descr",$value)) {
      $this->productDes=$value['descr'];
    }
    if (array_key_exists("category",$value)) {
      $this->category=$value['category'];
    }
    if (array_key_exists("subcategory",$value)) {
      $this->subcategory =$value['subcategory'];
    }
    if (array_key_exists("createAt",$value)) {
      $this->createdAt=$value['createAt'];
    }
    if (array_key_exists("updatedAt",$value)) {
      $this->updateAt=$value['updatedAt'];
    }
    return $this;
  }
  public function find_categorie($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from category WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_subcategorie($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from subcategory WHERE deleted_at='0000-00-00 00:00:00' AND categorie_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function insert($finalImageName='',$finalImageName2='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "INSERT INTO products_web (product_id,products_title,product_image,product_image2,product_des,price,previous_price,quantity,categorie,subcategory,instock,show_price,created_at) Values(:p,:pt,:i,:i2,:pd,:pr,:ppr,:q,:c,:sc,:s,:sp,:ca)";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'p' => $this->productId,
          'pt' => $this->title,
          'i' => $finalImageName,
          'i2' => $finalImageName2,
          'pd' => $this->productDes,
          'pr' => $this->price,
          'ppr' => $this->previousPrice,
          'q' => $this->quantity,
          'c' => $this->category,
          'sc' => $this->subcategory,
          's' =>'1',
          'sp' =>0,
          'ca' => $this->createdAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from products_web WHERE deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function find_product_categorie($productCategory)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from category WHERE categorie_id='$productCategory'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function show_single_products($value='')
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "SELECT * from products_web WHERE product_id='$value' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function update_without_image()
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  products_web SET products_title = :pt,product_des = :pd,price = :pp,previous_price = :ppr,updated_at = :ua WHERE product_id='$this->productId'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'pt' => $this->title,
          'pd' => $this->productDes,
          'pp' => $this->price,
          'ppr' => $this->previousPrice,
          'ua' => $this->updateAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function update_with_image($image,$image2)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  products_web SET products_title = :pt,product_image =:i,product_image2 =:i2,product_des =:pd,price = :pp,previous_price = :ppr,updated_at = :ua WHERE product_id='$this->productId'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'pt' => $this->title,
          'i' => $image,
          'i2' => $image2,
          'pd' => $this->productDes,
          'pp' => $this->price,
          'ppr' => $this->previousPrice,
          'ua' => $this->updateAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }
  public function delete($value='',$deletedAt)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  products_web SET deleted_at = :da WHERE product_id='$value'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'da' => $deletedAt
        ));
        $data = $stmt->fetchAll();
        return $data;
    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
