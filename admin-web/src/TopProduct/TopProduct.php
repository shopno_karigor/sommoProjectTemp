<?php
namespace App\TopProduct;
use PDO;
require_once '../../db/dbconnection.php';

class TopProduct
{
  private $imageNumber='';
  private $updateAt='';
  public function setData($value='')
  {
    if (array_key_exists("imageNumber",$value)) {
      $this->imageNumber=$value['imageNumber'];
    }
    if (array_key_exists("updatedAt",$value)) {
      $this->updateAt=$value['updatedAt'];
    }
    return $this;
  }
  public function update($imageName)
  {
    try{
        $pdo = new PDO('mysql:host=localhost;dbname='.db_name,db_username,db_password);
        $query = "UPDATE  top_product SET image = :i WHERE image_number='$this->imageNumber'";
        $stmt = $pdo->prepare($query);
        $stmt->execute(array(
          'i' => $imageName
        ));
        $data = $stmt->fetchAll();
        return $data;

    }catch (PDOException $e){
        echo 'Error: '. $e->getMessage();
    }
  }

}
 ?>
