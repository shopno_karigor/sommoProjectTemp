$(document).ready(function () {
  $('.delete-inbox').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Email!"))
    {
      var dataToSend = { mail: rowValue };
      $.post("../php/_delete-inbox.php", dataToSend, function(data, status){
          if(status === 'success')
          {
            location.reload();
          }
        });
    }
  });
});
