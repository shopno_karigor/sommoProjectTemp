
$(document).ready(function () {
  $('.add-special-offer').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Add for preview on Home Page !"))
    {
      var dataToSend = { addProductId: rowValue };

      $.post("../php/_special-offer-add-remove.php", dataToSend, function(data, status){
          if(status === 'success')
          {
            location.reload();
          }
        });

    }

  });

  $('.remove-special-offer').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Remove from preview on Home Page !"))
    {
      var dataToSend = { removeProductId: rowValue };
      $.post("../php/_special-offer-add-remove.php", dataToSend, function(data, status){
          if(status === 'success')
          {
            location.reload();
          }
        });

    }

  });


});
