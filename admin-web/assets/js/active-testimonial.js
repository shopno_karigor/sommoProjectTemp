
$(document).ready(function () {
  $('.show-testimonial').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Show this testimonial !"))
    {
      var dataToSend = { show: rowValue };
      $.post("../php/_active-testimonial.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             location.reload();
          }
        });
    }
  });

  $('.hide-testimonial').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Hide this testimonial !"))
    {
      var dataToSend = { hide: rowValue };
      $.post("../php/_active-testimonial.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             location.reload();
          }
        });
    }
  });

  $('.delete-testimonial').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete this testimonial !"))
    {
      var dataToSend = { testimonialId: rowValue };
      $.post("../php/_delete-testimonial.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }

  });
});
