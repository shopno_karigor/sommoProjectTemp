
$(document).ready(function () {
  $('.delete-banner').click(function (event) {
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Banner!"))
    {
      var dataToSend = { contentId: rowValue };
      $.post("../php/_delete-banner.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });
});
