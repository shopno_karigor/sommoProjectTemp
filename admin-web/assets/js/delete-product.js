
$(document).ready(function () {
  $('.delete-product').click(function (event) {
    // alert('lol');
    var _this = $(this);
    var rowValue = $(this).attr('data-row');
    if (window.confirm("Delete This Product !"))
    {
      var dataToSend = { contentId: rowValue };
      $.post("../php/_delete-product.php", dataToSend, function(data, status){
          if(status === 'success')
          {
             _this.parent().parent().remove();
          }
        });
    }
  });

});
