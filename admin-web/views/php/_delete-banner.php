<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Banner\Banner;
$b = new Banner();
$bannerId = $_POST['contentId'];
$yearMonthDay = date("Y-m-d");
$hourSecond = date("h:i:s");
$dateAndTime = $yearMonthDay." ".$hourSecond;
$b->delete($bannerId,$dateAndTime);
?>
