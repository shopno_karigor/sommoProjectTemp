<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Testimonial\Testimonial;
$b = new Testimonial();
$id = $_POST['testimonialId'];
$yearMonthDay = date("Y-m-d");
$hourSecond = date("h:i:s");
$dateAndTime = $yearMonthDay." ".$hourSecond;
$b->delete($id,$dateAndTime);
?>
