<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Banner\Banner;
$b = new Banner();
$row = $b->show_banner();
foreach ($row as $value) {
?>
<tr>
  <td class="">
    <img  src="<?php echo $baseUrl.'admin-web/assets/images/banner/'.$value['banner_image']?>" alt="" class="img-responsive center-block col-lg-10 col-md-10 col-sm-10 col-xs-12 p-b-sm">
  </td>
  <td class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
    <a class="btn btn-danger delete-banner" data-row="<?php echo $value['id']?>">Delete</a>
  </td>
</tr>
<?php
}
?>
