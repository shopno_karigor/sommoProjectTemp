<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Products\Products;
$productId =$_GET['product'];
$b = new Products();
$row = $b->show_single_products($productId);
if (!empty($row)) {
  $title =$row[0]['products_title'];
  $price =$row[0]['price'];
  $previousPrice =$row[0]['previous_price'];
  $priceOption =$row[0]['show_price'];
  $productDes =$row[0]['product_des'];
  $image =$row[0]['product_image'];
  $image2 =$row[0]['product_image2'];
  $productCategory = $row[0]['categorie'];
  $category = $b->find_product_categorie($productCategory);
  if (!empty($category)) {
    $productCategoryName =$category[0]['categorie_name'];
  }
}
?>
