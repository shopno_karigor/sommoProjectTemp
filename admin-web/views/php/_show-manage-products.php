<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Products\Products;
$b = new Products();
$row = $b->show_products();
foreach ($row as $value) {
?>
<tr>
  <td><?php echo $value['product_id'];?></td>
  <td><?php echo $value['products_title'];?></td>
  <td><?php echo $value['price'];?></td>
  <td><?php echo $value['previous_price'];?></td>
  <?php
    $productCategory = $value['categorie'];
    $category = $b->find_product_categorie($productCategory);
    if (!empty($category)) {
      echo '<td>'.$category[0]['categorie_name'].'</td>';
    }
   ?>
  <td><a href="update-product.php?product=<?php echo $value['product_id'] ?>" class="btn btn-success">Update</a></td>
  <td><a class="btn btn-danger delete-product" data-row="<?php echo $value['product_id'] ?>">Delete</a></td>
  <td><?php echo substr($value['created_at'],0,10);?></td>
</tr>
<?php
}
?>
