<?php
include_once ("../../vendor/autoload.php");
use App\Mail\Mail;
$b = new Mail();
$data = $b->show_mail_list();
if (!empty($data)) {
$firstInbox = $data[0]['mail_id'];
  foreach ($data as $row) {
    echo '<a href="inbox.php?id='.$row['mail_id'].'" >';
      echo '<div class="mail_list" >';
        echo '<div class="left">';
          echo '<i class="fa fa-circle-o"></i>';
        echo '</div>';
        echo '<div class="right">';
          echo '<h3>'.$row['name'].'</h3>';
          $shortMessage = substr($row['message'],0,50);
          echo '<p>'.$shortMessage.'...</p>';
        echo '</div>';
      echo '</div>';
    echo '</a>';
  }
}
else {
  $firstInbox = '';
}
 ?>
