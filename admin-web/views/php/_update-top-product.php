<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\TopProduct\TopProduct;
$b = new TopProduct();
if (isset($_FILES)) {
  if (empty($_FILES['image']['name']) || $_POST["imageNumber"]=='select') {
    $_SESSION['errorMessage']="Field data error";
    header("location:".$baseUrl."admin-web/views/layout/update-top-product.php");
  }
  else {
    $b->setData($_POST);
    $image = $_FILES['image']['name'];
    $image_loc =$_FILES['image']['tmp_name'];
    $temp = explode(".",$image);
    $imageName = 'top-product'.$_POST['imageNumber'].'.'.end($temp);
    if(move_uploaded_file($image_loc,"../../assets/images/top-product/".$imageName)){
      $result = $b->update($imageName);
      if (empty($result)) {
        $_SESSION['topProductSuccess']='Top Product Update Successful';
        header("location:".$baseUrl."admin-web/views/layout/update-top-product.php");
      }
    }else {
      $_SESSION['errorMessage']="Image Upload Failed";
      header("location:".$baseUrl."admin-web/views/layout/update-top-product.php");
    }
  }
}
 ?>
