<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Banner\Banner;
$b = new Banner();
if (isset($_FILES)) {
  if (empty($_FILES['image']['name'])) {
    $_SESSION['errorMessage']="Select image";
    header("location:".$baseUrl."admin-web/views/layout/add-banner.php");
  }
  else {
    $b->setData($_POST);
    $lastBanner = $b->find_last_banner();
    $lastBanner = $lastBanner+1;
    $image = $_FILES['image']['name'];
    $image_loc =$_FILES['image']['tmp_name'];
    $temp = explode(".",$image);
    $imageName = 'banner'.$lastBanner.'.'.end($temp);
    if(move_uploaded_file($image_loc,"../../assets/images/banner/".$imageName)){
      $result = $b->insert($imageName);
      if (empty($result)) {
        $_SESSION['bannerSuccess']='Banner Insert Successful';
        header("location:".$baseUrl."admin-web/views/layout/add-banner.php");
      }
    }else {
      $_SESSION['errorMessage']="Image Upload Failed";
      header("location:".$baseUrl."admin-web/views/layout/add-banner.php");
    }
  }

}
 ?>
