<?php
include_once ("../../vendor/autoload.php");
use App\Testimonial\Testimonial;
$yearMonthDay = date("Y-m-d");
$hourSecond = date("h:i:s");
$updatedAt = $yearMonthDay." ".$hourSecond;
$b = new Testimonial();
if (isset($_POST['show'])) {
  $b->active_testimonial($_POST['show']);
}
if (isset($_POST['hide'])) {
  $b->hide_testimonial($_POST['hide']);
}
 ?>
