<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Testimonial\Testimonial;
$b = new Testimonial();
if (isset($_POST)) {
  if ($_POST['descr'] == "<br>") {
    $_SESSION['errorMessage']="Description empty";
    header("location:".$baseUrl."admin-web/views/layout/add-testimonial.php");
  }
  else {
    $b->setData($_POST);
    $last= $b->find_last_testimonial();
    $last = $last+1;
    $testimonial_id = "t".$last;
    $result = $b->insert($testimonial_id);
    if (empty($result)) {
      $_SESSION['testimonialSuccess']='Testimonial Insert Successful';
      header("location:".$baseUrl."admin-web/views/layout/add-testimonial.php");
    }
  }
}
 ?>
