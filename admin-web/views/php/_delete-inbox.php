<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Mail\Mail;
$b = new Mail();
$yearMonthDay = date("Y-m-d");
$hourSecond = date("h:i:s");
$dateAndTime = $yearMonthDay." ".$hourSecond;
if (isset($_POST['mail'])) {
  $b->delete_mail($dateAndTime,$_POST['mail']);
}
?>
