<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Contact\Contact;
$b = new Contact();
if (isset($_POST)) {
  if (empty($_POST['address']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['fax']) || empty($_POST['facebookLink']) || empty($_POST['twitterLink']) || empty($_POST['hotline']) ) {
    $_SESSION['errorMessage']="Field Empty";
    header("location:".$baseUrl."admin-web/views/layout/update-contact.php");
  }
  else {
    $b->setData($_POST);
    $result = $b->update();
    if (empty($result)) {
      $_SESSION['contactSuccess']='Contact Update Successful';
      header("location:".$baseUrl."admin-web/views/layout/update-contact.php");
    }
  }
}
 ?>
