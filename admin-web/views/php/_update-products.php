<?php
session_start();
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Products\Products;
$b = new Products();
if (isset($_POST)) {
  if (empty($_POST['title']) || empty($_POST['price']) || $_POST['descr']=="<br>") {
    $_SESSION['errorMessage']="Field Empty";
    header("location:".$baseUrl."admin-web/views/layout/update-product.php?product=".$_POST['productId']);
  }
  else {
    $b->setData($_POST);
    if (empty($_FILES['image']['name']) || empty($_FILES['image2']['name'])) {
      $result = $b->update_without_image();
      if (empty($result)) {
        $_SESSION['productSuccess']='Product Update Successful';
        header("location:".$baseUrl."admin-web/views/layout/update-product.php?product=".$_POST['productId']);
      }
    }else {
      $image = $_FILES['image']['name'];
      $image_loc =$_FILES['image']['tmp_name'];
      $temp = explode(".",$image);
      $finalImageName = $_POST['productId'].'.'.end($temp);

      $image2 = $_FILES['image2']['name'];
      $image_loc2 =$_FILES['image2']['tmp_name'];
      $temp2 = explode(".",$image2);
      $finalImageName2 = $_POST['productId'].'-2.'.end($temp2);

      if(move_uploaded_file($image_loc,"../../assets/images/products/".$finalImageName)){
        move_uploaded_file($image_loc2,"../../assets/images/products/".$finalImageName2);
        $result = $b->update_with_image($finalImageName,$finalImageName2);
        if (empty($result)) {
          $_SESSION['productSuccess']='Product Update Successful';
          header("location:".$baseUrl."admin-web/views/layout/update-product.php?product=".$_POST['productId']);
        }
      }else {
        $_SESSION['errorMessage']="Product Image Upload Failed";
        header("location:".$baseUrl."admin-web/views/layout/update-product.php?product=".$_POST['productId']);
      }
    }
  }
}
 ?>
