<?php
include_once ("../../vendor/autoload.php");
use App\Mail\Mail;
if (array_key_exists("id",$_GET)) {
  $showInboxId = $_GET['id'];
}
else {
  $showInboxId = $firstInbox;
}
$b = new Mail();
$row = $b->showInboxBody($showInboxId);
if (!empty($row)) {
  $monthArray  = array('-01-' => 'JAN', '-02-' => 'FEB', '-03-' => 'MAR', '-04-' => 'APR', '-05-' => 'MAY', '-06-' => 'JUN', '-07-' => 'JUL', '-08-' => 'AUG', '-09-' => 'SEP', '-10-' => 'OCT', '-11-' => 'NOV', '-12-' => 'DEC');

    $inboxId = $row[0]['mail_id'];
    $name = $row[0]['name'];
    $email = $row[0]['email'];
    $phone = $row[0]['phone'];
    $message = $row[0]['message'];
    $date = $row[0]['created_at'];
    $year = substr($date,0,4);
    $m = substr($date,4,4);
    $month = $monthArray[$m];
    $day = substr($date,8,-8);
    $time = substr($date,10,-3);
    $finalDate = $time.", ".$day." ".$month." ".$year;
}
else {
  $inboxId = '';
  $name = '';
  $email = '';
  $phone = '';
  $message = '';
  $date = '';
  $finalDate = '';
}
 ?>
