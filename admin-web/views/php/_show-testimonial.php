<?php
include_once ("_header.php");
include_once ("../../vendor/autoload.php");
use App\Testimonial\Testimonial;
$b = new Testimonial();
$row = $b->show_testimonial();
foreach ($row as $value) {
?>
<tr>
  <td><?php echo $value['description'];?></td>
  <td><?php echo $value['authore'];?></td>
  <?php
    if ($value['view']=='0') {
      echo '<td><a data-row="'.$value['testimonial_id'].'" class="btn btn-success show-testimonial" >Show</a></td>';
    }else {
      echo '<td><a data-row="'.$value['testimonial_id'].'" class="btn btn-danger hide-testimonial" >Hide</a></td>';
    }
   ?>
  <td><a class="btn btn-danger delete-testimonial" data-row="<?php echo $value['testimonial_id'];?>">Delete</a></td>
</tr>
<?php
}
?>
