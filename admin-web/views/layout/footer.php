<!-- jQuery -->
<script src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../../assets/js/nprogress.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../assets/js/moment/moment.min.js"></script>
<script src="../../assets/js/datepicker/daterangepicker.js"></script>


<!-- Custom Theme Scripts -->
<script src="../../assets/js/custom.min.js"></script>

<?php if($page_slug == "home" || $page_slug =="change-date") { ?>
  <script type="text/javascript" src="../../assets/js/date.js"></script>
<?php }?>

<?php if ($page_slug == "testimonial-list"){ ?>
  <script type="text/javascript" src="../../assets/js/active-testimonial.js"></script>
  <!-- Datatables -->
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
  <script src="../../assets/js/dataTables.buttons.min.js"></script>
  <script src="../../assets/js/buttons.bootstrap.min.js"></script>

  <script src="../../assets/js/dataTables.responsive.min.js"></script>
  <script src="../../assets/js/responsive.bootstrap.js"></script>

  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      $('#datatable').dataTable();
      $('#datatable-responsive').DataTable();
      TableManageButtons.init();
    });
  </script>
  <!-- /Datatables -->
<?php } ?>
<?php if ($page_slug == "manage-banner"){ ?>
  <script type="text/javascript" src="../../assets/js/delete-banner.js"></script>
<?php } ?>
<?php if ($page_slug == "add-product"){ ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#checkProductId").keyup(function() {
        var productId = $("#checkProductId").val();
        if (productId) {
          var dataToSend = { content: productId };
          $.post("../php/_check-product-id.php", dataToSend, function(data, status){
              if(status === 'success')
              {
                if (data) {
                  $("#errorProductId").css("display","block")
                  $("#checkProductId").css("border","1px solid red")
                }
                else {
                  $("#errorProductId").css("display","none")
                  $("#checkProductId").css("border","1px solid #333")
                }
              }
            });
        }
      });
    });
  </script>
<?php } ?>

<?php if ($page_slug == "manage-products"){ ?>
  <script type="text/javascript" src="../../assets/js/delete-product.js"></script>
  <!-- Datatables -->
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
  <script src="../../assets/js/dataTables.buttons.min.js"></script>
  <script src="../../assets/js/buttons.bootstrap.min.js"></script>

  <script src="../../assets/js/dataTables.responsive.min.js"></script>
  <script src="../../assets/js/responsive.bootstrap.js"></script>

  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      $('#datatable').dataTable();
      $('#datatable-responsive').DataTable();
      TableManageButtons.init();
    });
  </script>
  <!-- /Datatables -->
<?php } ?>
<?php if ($page_slug == "inbox"){ ?>
  <script src="../../assets/js/delete-inbox.js"></script>
<?php }?>
<?php if ($page_slug == "subscriber-list"){ ?>
  <script type="text/javascript" src="../../assets/js/update-to-delevery.js"></script>
  <!-- Datatables -->
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
  <script src="../../assets/js/dataTables.buttons.min.js"></script>
  <script src="../../assets/js/buttons.bootstrap.min.js"></script>

  <script src="../../assets/js/datatables.net-buttons/js/buttons.flash.js"></script>
  <script src="../../assets/js/datatables.net-buttons/js/buttons.html5-sub.js"></script>
  <script src="../../assets/js/datatables.net-buttons/js/buttons.print-sub.js"></script>
  <script src="../../assets/js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="../../assets/js/jszip/dist/jszip.min.js"></script>

  <script src="../../assets/js/dataTables.responsive.min.js"></script>
  <script src="../../assets/js/responsive.bootstrap.js"></script>

  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      var handleDataTableButtons = function() {
        if ($("#datatable-buttons").length) {
          $("#datatable-buttons").DataTable({
            dom: "Bfrtip",
            buttons: [
              {
                extend: "copy",
                className: "btn-sm"
              },
              {
                extend: "excel",
                className: "btn-sm"
              },
              {
                extend: "print",
                className: "btn-sm"
              },
            ],
            responsive: true
          });
        }
      };

      TableManageButtons = function() {
        "use strict";
        return {
          init: function() {
            handleDataTableButtons();
          }
        };
      }();

      $('#datatable-responsive').DataTable();

      var table = $('#datatable-fixed-header').DataTable({
        fixedHeader: true
      });

      TableManageButtons.init();
    });
  </script>
  <!-- /Datatables -->
<?php } ?>
<?php if ($page_slug == "add-product"){ ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#productCategory').change(function() {
        var id = $('#productCategory').find(":selected").attr("value");
        var dataToSend = { categoryId: id };
        $.post("../php/_find-product-subcategories.php", dataToSend, function(data, status){
            if(status === 'success')
            {
              $("#productSubategory").html(data);
            }
          });
      });
    });
  </script>
<?php } ?>
<!-- bootstrap-wysiwyg -->
<script src="../../assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="../../assets/js/jquery.hotkeys.js"></script>
<script src="../../assets/js/prettify.js"></script>

<!-- bootstrap-wysiwyg -->
<script>
  $(document).ready(function() {
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
          'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
          'Times New Roman', 'Verdana'
        ],
        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function(idx, fontName) {
        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
      });
      $('a[title]').tooltip({
        container: 'body'
      });
      $('.dropdown-menu input').click(function() {
          return false;
        })
        .change(function() {
          $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
        })
        .keydown('esc', function() {
          this.value = '';
          $(this).change();
        });

      $('[data-role=magic-overlay]').each(function() {
        var overlay = $(this),
          target = $(overlay.data('target'));
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });

      if ("onwebkitspeechchange" in document.createElement("input")) {
        var editorOffset = $('#editor').offset();

        $('.voiceBtn').css('position', 'absolute').offset({
          top: editorOffset.top,
          left: editorOffset.left + $('#editor').innerWidth() - 35
        });
      } else {
        $('.voiceBtn').hide();
      }
    }

    function showErrorAlert(reason, detail) {
      var msg = '';
      if (reason === 'unsupported-file-type') {
        msg = "Unsupported format " + detail;
      } else {
        console.log("error uploading file", reason, detail);
      }
      $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }

    initToolbarBootstrapBindings();

    $('#editor').wysiwyg({
      fileUploadError: showErrorAlert
    });

    window.prettyPrint;
    prettyPrint();
  });
</script>
<!-- /bootstrap-wysiwyg -->

<script type="text/javascript">
  function textEditor() {
    var content = document.getElementById('editor').innerHTML;
    // alert(content);
    var finalContent = content.replace("'", "-");
    document.getElementById('descr').innerHTML=finalContent;
    // alert(content);
  }
</script>

<script type="text/javascript">
  function wordCount() {
    var maxWord=800;
    var word=document.getElementById('editor').innerHTML;
    var ln = word.length;
    var subWord =word.slice(0,4);
    var subWord2 =word.slice(ln-4,ln);
    if (subWord=='<br>') {
      word = word.substr(5, ln);
    }
    if (subWord2=='<br>') {
      word = word.substr(0, ln-4);
    }
    // alert(word);
    var ln = word.length;
    if (ln==0) {
      document.getElementById('wordRemaining').innerHTML=maxWord+" More character's";
    }
    else if (ln>maxWord) {
      var finalString = document.getElementById('editor').innerHTML;
      // document.getElementById('editor').disabled = true;
      finalString = finalString.substr(0,maxWord)
      document.getElementById('editor').innerHTML=finalString;
      document.getElementById('wordRemaining').innerHTML="0  character";
    }
    else {
      var remainWord=maxWord-ln;

      if (remainWord>=1) {
        document.getElementById('wordRemaining').innerHTML=remainWord+" More character's";
      }
      if (remainWord==0) {
        document.getElementById('wordRemaining').innerHTML="0  character";

      }
      if (remainWord<0) {
        document.getElementById('editor').disabled = true;
      }

    }

  }
</script>

<script type="application/javascript">
    $(document).ready(function () {

      var fullDate = new Date();
      var date = fullDate.getDate();
      if (date<10) {
        date = date.toString();
        date = '0'+date;
      }
      else {
        date = date.toString();
      }


      // finding month
      var month  = fullDate.getMonth();
      month = month +1;
      if (month<10) {
        month = month.toString();
        month = '0'+month;
      }
      else {
        month = month.toString();
      }
      // alert(month);

      // finding year
      var year = fullDate.getYear();
        year = year.toString();
      var yearSlice = year.slice(1);


      var Daydate = yearSlice+'/'+month+'/'+date;
      var PostDate = yearSlice+month+date;

      document.getElementById('newsDate').value = Daydate;
      document.getElementById('postDate').value = PostDate;


    });
</script>

<!-- footer content -->
<footer>
  <div class="pull-right">
    <?php echo $websiteName ?> - Bootstrap Admin Template
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->

      </div>
    </div>
  </body>
</html>
