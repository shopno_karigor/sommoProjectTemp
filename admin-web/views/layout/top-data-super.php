<?php include '../php/_top-data-super.php'; ?>
 <!-- top tiles -->
 <div class="row tile_count">
   <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top green"><i class="fa fa-shopping-basket"></i> Total Products</span>
     <div class="count green"><?php echo $totalProducts; ?></div>
   </div>
   <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top purple"><i class="fa fa-bullhorn"></i> Total Subscribers</span>
     <div class="count purple"><?php echo $totalSub ?></div>
   </div>
   <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
     <span class="count_top light-blue"><i class="fa fa-envelope-open"></i> Today Message</span>
     <div class="count light-blue"><?php echo $emailToday ?></div>
   </div>
 </div>
 <!-- /top tiles -->
