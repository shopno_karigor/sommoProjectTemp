            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="home.php">Dashboard</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-picture-o"></i>Banner Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="add-banner.php">Add Banner</a></li>
                      <li><a href="manage-banner.php">Manage Banner</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-picture-o"></i>Top Product Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="update-top-product.php">Update Top Product</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-rss"></i>Testimonial Manager<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="add-testimonial.php">Add Testimonial</a></li>
                      <li><a href="testimonial-list.php">Testimonial List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-id-card-o"></i>Contact Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="update-contact.php">Update Contact</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-envelope-open"></i>Inbox<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="inbox.php">Mail List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bullhorn"></i>Subscriber Manager<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="subscriber.php">Subscriber List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-shopping-basket"></i>Products Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="add-product.php">Add Products</a></li>
                      <li><a href="manage-products.php">Manage Products</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu
