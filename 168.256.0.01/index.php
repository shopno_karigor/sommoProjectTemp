<?php
session_start();
include "../views/php/_header.php";
$page_slug="302";
?>
 <!DOCTYPE HTML>
 <html>
 	<head>
 		<title>Online Sell Platform</title>
    <link href="<?php echo $baseUrl.'assets/images/icon1.png'; ?>" rel="icon" type="image/x-icon" />
    <meta name="keywords" content="" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
		<link href="<?php echo $baseUrl.'assets/css/style.css'; ?>" rel="stylesheet" type="text/css"  media="all" />
 		<link href='//fonts.googleapis.com/css?family=Londrina+Solid|Coda+Caption:800|Open+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo $baseUrl.'assets/css/style1.css'; ?>" rel="stylesheet" />
</head>
<body>
<div class="clear"> </div>
<div class="wrap">
<div class="content">
<div class="content-grids-error">
	<div class="error-page">
		<h4 style="margin-top:150px;">Welcome</h4>
		<h5>Online Sell Platform</h5>
	</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2 col-md-2 col-sm-2 col-lg-offset-4">
			<a href="<?php echo $baseUrl.'admin'; ?>" class="btn btn-primary" style="font-family: 'Ubuntu', sans-serif;">Software Admin</a>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2">
			<a href="<?php echo $baseUrl.'admin-web'; ?>" class="btn btn-success" style="font-family: 'Ubuntu', sans-serif;">&nbsp;Website Admin</a>
		</div>
	</div>
</div>
<div class="clear"> </div>
</div>
