-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 04:50 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sommo_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL,
  `subcategory_id` text NOT NULL,
  `categorie_id` text NOT NULL,
  `subcategory_name` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `subcategory_id`, `categorie_id`, `subcategory_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'c1s1', 'c1', 'Men Shirts', '2018-05-30 08:50:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'c2s2', 'c2', 'Women Shirts', '2018-05-30 10:50:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'c3s3', 'c3', 'New Born Dress', '2018-05-30 10:50:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'c2s4', 'c2', 'Shari', '2018-05-30 11:20:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'c2s5', 'c2', 'Lehenga', '2018-05-30 11:20:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'c2s6', 'c2', 'Women Jeans', '2018-05-30 11:21:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'c2s7', 'c2', 'Women Slipers', '2018-05-30 11:21:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'c2s8', 'c2', 'Party Dress', '2018-05-30 11:21:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'c3s9', 'c3', 'Kids Slipers', '2018-05-30 11:22:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'c1s10', 'c1', 'Men Pants', '2018-05-30 11:22:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'c1s11', 'c1', 'Blazer', '2018-05-30 11:22:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'c1s12', 'c1', 'Panjabi', '2018-05-30 11:22:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'c1s13', 'c1', 'Complite Sute', '2018-05-30 11:22:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'c1s14', 'c1', 'Men Slipers', '2018-05-30 11:23:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'c3s15', 'c3', 'Kids Shirts', '2018-05-30 11:23:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'c3s16', 'c3', 'Kids Panjabi', '2018-05-30 11:23:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'c3s17', 'c3', 'Kids Shoes', '2018-05-30 11:23:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'c3s18', 'c3', 'Kids Pants', '2018-05-30 11:24:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
