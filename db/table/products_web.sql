-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 04:50 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sommo_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `products_web`
--

CREATE TABLE `products_web` (
  `id` int(11) NOT NULL,
  `product_id` text NOT NULL,
  `products_title` text NOT NULL,
  `product_image` text NOT NULL,
  `product_image2` text NOT NULL,
  `product_des` text NOT NULL,
  `price` text NOT NULL,
  `previous_price` text NOT NULL,
  `quantity` text NOT NULL,
  `categorie` text NOT NULL,
  `subcategory` text NOT NULL,
  `instock` int(1) NOT NULL,
  `show_price` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_web`
--

INSERT INTO `products_web` (`id`, `product_id`, `products_title`, `product_image`, `product_image2`, `product_des`, `price`, `previous_price`, `quantity`, `categorie`, `subcategory`, `instock`, `show_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'c1p5', 'Men''s Collection', 'c1p5.jpg', 'c1p5-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c1', 'c1s10', 1, 0, '2018-05-17 05:34:48', '2018-05-17 05:37:13', '0000-00-00 00:00:00'),
(6, 'c1p6', 'Men''s Collection', 'c1p6.jpg', 'c1p6-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n', '10', '10', '10', 'c1', 'c1s1', 1, 0, '2018-05-17 05:35:31', '2018-05-27 05:12:25', '0000-00-00 00:00:00'),
(7, 'c1p7', 'Men''s Collection', 'c1p7.jpg', 'c1p7-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n', '10', '10', '10', 'c1', 'c1s13', 1, 0, '2018-05-17 05:39:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'c1p8', 'Men''s Shirts', 'c1p8.jpg', 'c1p8-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n', '10', '10', '10', 'c1', 'c1s14', 1, 0, '2018-05-17 05:39:56', '2018-07-16 02:35:34', '0000-00-00 00:00:00'),
(19, 'c1p19', 'Women''s Collection', 'c1p19.jpg', 'c1p19-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c2', 'c2s2', 1, 0, '2018-05-24 05:29:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'c1p24', 'Women''s Collection', 'c1p24.jpg', 'c1p24-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c2', 'c2s5', 1, 0, '2018-05-24 05:35:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'c1p25', 'Women''s Collection', 'c1p25.jpg', 'c1p25-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c2', 'c2s6', 1, 0, '2018-05-24 05:36:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'c1p26', 'Women''s Collection', 'c1p26.jpg', 'c1p26-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c2', 'c2s7', 1, 0, '2018-05-24 05:37:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'c1p37', 'Kid''s Collection', 'c1p37.jpg', 'c1p37-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c3', 'c3s15', 1, 0, '2018-05-24 05:46:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'c1p38', 'Kid''s Collection', 'c1p38.jpg', 'c1p38-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c3', 'c3s16', 1, 0, '2018-05-24 05:47:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'c1p39', 'Kid''s Collection', 'c1p39.jpg', 'c1p39-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c3', 'c3s17', 1, 0, '2018-05-24 05:48:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'c1p40', 'Kid''s Collection', 'c1p40.jpg', 'c1p40-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '10', '10', '10', 'c3', 'c3s18', 1, 0, '2018-05-24 05:49:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '00d41', 'Test', '00d41.jpg', '00d41-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '200', '200', '5', 'c1', 'c1s11', 1, 0, '2018-07-16 02:26:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products_web`
--
ALTER TABLE `products_web`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products_web`
--
ALTER TABLE `products_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
