-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 04:52 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sommo_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `user_name` text NOT NULL,
  `email` text NOT NULL,
  `user_pass` text NOT NULL,
  `user_hash` text NOT NULL,
  `status` text NOT NULL,
  `token` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_name`, `email`, `user_pass`, `user_hash`, `status`, `token`, `created_at`, `deleted_at`, `updated_at`) VALUES
(2, 'user2', 'ratul', 'ratul@techkarigar.com', '$2y$10$ODM0ZDMxZjExOWNkYTJiN.N2infn..DD2hDR3/fXNlprTX8WmraBW', '$2y$10$ODM0ZDMxZjExOWNkYTJiND', 'Prime', 0, '2017-06-01 12:30:09', '0000-00-00 00:00:00', '2018-05-04 06:16:15'),
(3, 'user3', 'sommo', 'admin@onlinesell.com', '$2y$10$ZGNiNWYyMDgwM2JkZTU4N.nlFiF1bFPiqhzem.lre0ibz1buPyUHK', '$2y$10$ZGNiNWYyMDgwM2JkZTU4ND', 'Super', 0, '2017-06-01 12:30:25', '0000-00-00 00:00:00', '2017-06-09 06:54:13'),
(11, 'user11', 'Anis', 'anis@onlinesell.com', '$2y$10$MDI0OWJiMWVmOTIxMmRlMue8cMai.6EG1FWcRSBHjUVWX7xTwL2v.', '$2y$10$MDI0OWJiMWVmOTIxMmRlMz', 'Moderator', 0, '2017-06-09 11:17:41', '0000-00-00 00:00:00', '2018-05-27 12:53:26'),
(12, 'user12', 'Jibon', 'jibon@onlinesell.com', '$2y$10$ODA3NDJjN2QyZDhlYjI3ZOsYEtI6Fm8mR44B30ckmqtXa4KUfhkWi', '$2y$10$ODA3NDJjN2QyZDhlYjI3ZT', 'Moderator', 0, '2017-06-09 02:13:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'user13', 'Jaman', 'jaman@onlinesell.com', '$2y$10$ODg5Yjc0YmE4MzkxMDQzNuYLM7BcRlVsGR8kMLeAl54y9mqVyeiwu', '$2y$10$ODg5Yjc0YmE4MzkxMDQzNz', 'Moderator', 0, '2017-06-09 02:15:25', '0000-00-00 00:00:00', '2018-06-28 03:53:20'),
(14, 'user14', 'Limon', 'limon@onlinesell.com', '$2y$10$MjQ1ZTdlYTcwMjMxNzhmYObLpPHBXE8lu87MgO/UE7Y2R5TdO2oQu', '$2y$10$MjQ1ZTdlYTcwMjMxNzhmYT', 'Moderator', 0, '2017-06-09 02:28:40', '0000-00-00 00:00:00', '2018-05-04 06:15:04'),
(15, 'user15', 'Ornob', 'ornob@onlinesell.com', '$2y$10$MmE4OWYxODQ2YzEwN2Q0ZebAAuGfWw9NACO8RaA6DIcNgANPTbBhO', '$2y$10$MmE4OWYxODQ2YzEwN2Q0Zj', 'Moderator', 0, '2017-07-16 09:33:06', '0000-00-00 00:00:00', '2018-06-24 03:58:11'),
(16, 'user16', 'Sathi', 'sathi@onlinesell.com', '$2y$10$YWM0YzY2Yjk3NmEzYjVmNOzbOGOeTyUzbyVi2baMPVhJcDkS9.Fp2', '$2y$10$YWM0YzY2Yjk3NmEzYjVmNW', 'Moderator', 0, '2018-05-03 03:33:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
