$(document).ready(function () {
  $('.add-item').click(function (event) {
    var _this = $(this);
    var product = $(this).attr('data-item');
    var dataToSend = { itemId: product};
    $.post('http://localhost/sommoFinalProject/views/php/_add-to-cart.php', dataToSend, function (data,status) {
      if (status === "success") {
        alert("Product add to cart");
      }
    });
  });
  $(document).on("click",".remove-cart",function (event) {
    var _this = $(this);
    var item = $(this).attr("data-item");
    var dataToSend = { itemId: item };
    $.post('http://localhost/sommoFinalProject/views/php/_delete-to-cart.php', dataToSend, function (data,status) {
      if (status === "success") {
        _this.parent().parent().remove();
      }
    });
  });
});
