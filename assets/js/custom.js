$(document).ready(function() {
  $(".has-sub").mouseover(function() {
    var _this = $(this);
    // console.log(_this);
    var status = _this.attr("data-stat");
    if (status=="close") {
      _this.attr("data-stat","open");
      $(this).children("a span").removeClass("fa-chevron-down");
      $(this).children("a span").addClass("fa-chevron-up");
      $(this).children("ul").removeClass("has-close");
      $(this).children("ul").addClass("sub-open");
    }
  });
  $(".has-sub").mouseout(function() {
    var _this = $(this);
    // console.log(_this);
    var status = _this.attr("data-stat");
    if (status=="open") {
      _this.attr("data-stat","close");
      $(this).children("a span").removeClass("fa-chevron-up");
      $(this).children("a span").addClass("fa-chevron-down");
      $(this).children("ul").removeClass("sub-open");
      $(this).children("ul").addClass("has-close");
    }
  });

});
